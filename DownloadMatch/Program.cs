﻿using HtmlAgilityPack;
using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Data.Objects;
using ParisEntreAmis.Service;
using ParisEntreAmis.Repository;


namespace DownloadMatch
{

    public class PgmContext : IDBContext
    {
        private AppContext db = new AppContext();


        public AppContext GetContext()
        {
            return db;
        }
    }

    class Program
    {


        static void Main(string[] args)
        {

            //BDD
            PgmContext pgmContext = new PgmContext();

            //Les repository
            ITeamRepository teamRepo = new TeamRepository(pgmContext);
            IPlayerRepository playerRepo = new PlayerRepository(pgmContext);

            //Les services
          /*  UpdateTeamService updateTeamService = new UpdateTeamService(teamRepo,playerRepo,new LogConsoleService());


            updateTeamService.UpdateTeamsForCountryAndCategory("FR", "Ligue 1", "http://www.sports.fr/football/ligue-1/clubs.html");
            updateTeamService.UpdateTeamsForCountryAndCategory("DE", "Bundesligua", "http://www.sports.fr/football/allemagne/clubs.html");
            updateTeamService.UpdateTeamsForCountryAndCategory("EN", "", "http://www.sports.fr/football/angleterre/clubs.html");
            updateTeamService.UpdateTeamsForCountryAndCategory("ES", "", "http://www.sports.fr/football/espagne/clubs.html");
            updateTeamService.UpdateTeamsForCountryAndCategory("IT", "", "http://www.sports.fr/football/italie/clubs.html");
            */

            
            ParseAllDataLigue1Fr(34);

            //Allemagne
           // ParseAllDataLigue1De(1);

            //Angleterre
           // ParseAllDataLigue1En(1);
       

            //ParseAllDataLdc();


            Console.WriteLine("press any key to close");
            Console.ReadKey();

        }

        


        private static void ParseAllDataLdc(int start)
        {
            // http://www.sports.fr/football/ligue-des-champions/2014/resultats/6e-journee.html


            for (int i = start; i <= 6; i++)
            {
                Console.WriteLine("##### LDC  Début insertion journée " + i);
                ParseOneDay("http://www.sports.fr/football/ligue-des-champions/2014/resultats/" + i + "e-journee.html", i, "LDC", "LDC");
                Console.WriteLine("##### LDC Insertion journée " + i + " terminée");
            }

        }

        private static void ParseAllDataLigue1De(int start)
        {
            // http://www.sports.fr/football/allemagne/2014/resultats/1e-journee.html


            //On insere les 34 journées de ligue 1
            for (int i = start; i <= 34; i++)
            {
                Console.WriteLine("##### ALLEMAGNE Début insertion journée " + i);
                ParseOneDay("http://www.sports.fr/football/allemagne/2014/resultats/" + i + "e-journee.html", i, "DE" , "Ligue 1");
                Console.WriteLine("##### ALLEMAGNE Insertion journée " + i + " terminée");
            }

        }

        private static void ParseAllDataLigue1En(int start)
        {
            // http://www.sports.fr/football/angleterre/2014/resultats/1e-journee.html


            //On insere les 34 journées de ligue 1
            for (int i = start; i <= 34; i++)
            {
                Console.WriteLine("##### ANGLETERRE Début insertion journée " + i);
                ParseOneDay("http://www.sports.fr/football/angleterre/2014/resultats/" + i + "e-journee.html", i, "EN", "Ligue 1");
                Console.WriteLine("##### ANGLETERRE Insertion journée " + i + " terminée");
            }

        }

        private static void ParseAllDataLigue1Fr(int start)
        {
            // http://www.sports.fr/football/ligue-1/2014/resultats/1e-journee.html


            //On insere les 38 journées de ligue 1
            for (int i = start; i <= 38;i++ )
            {
                Console.WriteLine("##### FRANCE Début insertion journée " + i);
                ParseOneDay("http://www.sports.fr/football/ligue-1/2014/resultats/" + i + "e-journee.html",i, "FR" , "Ligue 1");
                Console.WriteLine("##### FRANCE Insertion journée " + i+" terminée");
            }
            
        }

        private static void ParseOneDay(string url, int day , string country , string division)
        {
            // http://www.sports.fr/football/ligue-1/2014/resultats/1e-journee.html

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);

            

            PgmContext pgmContext = new PgmContext();
            AppContext db = pgmContext.GetContext();

            //On choppe la table avec les résultats

            HtmlNode table = doc.DocumentNode.SelectNodes("//table[@class='nwResultats']").FirstOrDefault();

            //On parcours tous les tr
            foreach (HtmlNode line in table.SelectNodes("tbody/tr"))
            {

                List<MatchGoal> scorersHomeTeam = new List<MatchGoal>();
                List<MatchGoal> scorersAwayTeam = new List<MatchGoal>();

                Team homeTeam = null;
                Team awayTeam = null;

                int? scoreHT = null;
                int? scoreAT = null;

                //On choppe les TD
                var allsTds = line.SelectNodes("td");

                //1er TD correspond à la date du match
                HtmlNode tdDate = allsTds[0];
                string dateStr = tdDate.InnerText;

                //2eme td correspond à l'heure
                HtmlNode tdHour = allsTds[1];
                string hourStr = tdHour.InnerText;

                DateTime dateMatch = DateTime.ParseExact(dateStr+" "+hourStr.Replace("h",":"), "dd/MM/yy H:m", CultureInfo.InvariantCulture);


                //4eme td correspond à l'équipe 1
                HtmlNode tdHomeTeam = allsTds[3];

                //On choppe le lien permettant de trouver l'ID de l'équipe
                var contenuTd1 = tdHomeTeam.SelectNodes("a");
                if (contenuTd1 == null) //Quand équipe gagnante la balise a est entre balise b
                {
                    contenuTd1 = tdHomeTeam.SelectNodes("b/a");
                }

                HtmlNode a = contenuTd1.FirstOrDefault();


                string link = a.Attributes["href"].Value;

                Regex myRegex = new Regex(@"(.*)-(.*)\.html");
                System.Text.RegularExpressions.Match m = myRegex.Match(link);
                if (m.Success)
                {
                    int id = int.Parse(m.Groups[2].Value);

                    //Get the Team or create it 
                    homeTeam = GetAndInsertTeam(db, id, link);
                }
                
                //5eme TD correspond au score
                HtmlNode tdScore = allsTds[4];
                string[] score = tdScore.InnerText.Trim().Split('-');
                if (score != null && score.Length == 2)
                {
                    scoreHT = int.Parse(score[0]);
                    scoreAT = int.Parse(score[1]);
                }

                //6eme td correspond à l'équipe 2
                HtmlNode tdAwayTeam = allsTds[5];

                //On choppe le lien permettant de trouver l'ID de l'équipe

                var contenuTd = tdAwayTeam.SelectNodes("a");
                if (contenuTd == null) //Quand équipe gagnante la balise a est entre balise b
                {
                    contenuTd = tdAwayTeam.SelectNodes("b/a");
                }

                HtmlNode a2 = contenuTd.FirstOrDefault();


                string link2 = a2.Attributes["href"].Value;

                System.Text.RegularExpressions.Match m2 = myRegex.Match(link2);
                if (m2.Success)
                {
                    int id = int.Parse(m2.Groups[2].Value);

                    //Get the Team or create it 
                    awayTeam = GetAndInsertTeam(db, id, link2);
                }


                ParisEntreAmis.Models.Match match = new ParisEntreAmis.Models.Match();
                match.AwayTeam = awayTeam;
                match.HomeTeam = homeTeam;
                match.Country = country;
                match.Div = division;
                match.ScoreAwayTeam = scoreAT;
                match.ScoreHomeTeam = scoreHT;
                match.Date = dateMatch;
                match.Day = day;
                match.Url = url;

                //On ajoute que si le match n'existe pas déjà.
                //On check par rapport à la journée car des dates peuvent changer de date

                ParisEntreAmis.Models.Match matchDB = db.Matchs.Include("MatchGoals").Where(t => t.Day == day && t.AwayTeam.TeamID == match.AwayTeam.TeamID && t.HomeTeam.TeamID == match.HomeTeam.TeamID).FirstOrDefault();

                if (matchDB == null)
                {
                    match.MatchGoals = new List<MatchGoal>();
                    matchDB = db.Matchs.Add(match);

                    db.SaveChanges();
                   
                    Console.WriteLine("match "+matchDB.MatchID+" inséré");
                }
                else
                {
                    match.MatchID = matchDB.MatchID;

                    Console.WriteLine(" -- > MATCH DEJA EXISTANT, ON UPDATE LES INFOS");
                    matchDB.ScoreAwayTeam = match.ScoreAwayTeam;
                    matchDB.ScoreHomeTeam = match.ScoreHomeTeam;
                    matchDB.Date = match.Date;
                    matchDB.Country = match.Country;
                    matchDB.Div = match.Div;
                    matchDB.Day = day;
                    matchDB.Url = url;
                    db.Entry(matchDB).State = System.Data.EntityState.Modified;
                }

              


                 //On cherche les buteurs
                //Ils se trouvent dans les balises <span>
                HtmlNode nodeScorersHomeTeam = tdHomeTeam.SelectNodes("small")!=null?tdHomeTeam.SelectNodes("small").FirstOrDefault():null;
                
                if (nodeScorersHomeTeam != null) //Si non vide signifie qu'il y a pas de buteurs
                {
                    //On choppe tous les a
                    scorersHomeTeam = ParseAndCreateListMatchGoals(nodeScorersHomeTeam,homeTeam.TeamID , matchDB.MatchID );
                }

                //On cherche les buteurs
                //Ils se trouvent dans les balises <span>
                HtmlNode nodeScorersAwayTeam = tdAwayTeam.SelectNodes("small") != null ? tdAwayTeam.SelectNodes("small").FirstOrDefault() : null;

                if (nodeScorersAwayTeam != null) //Si non vide signifie qu'il y a pas de buteurs
                {
                    //On choppe tous les a
                    scorersAwayTeam = ParseAndCreateListMatchGoals(nodeScorersAwayTeam, awayTeam.TeamID, matchDB.MatchID);
                }

                scorersAwayTeam.AddRange(scorersHomeTeam);

                //On vérifie si les joueurs existent vraiment
                foreach (var item in scorersAwayTeam)
                {
                    if (db.Players.Find(item.PlayerID) == null)
                    {

                        //On créé la fake team pr les fake joueurs
                        if (db.Teams.Find(0) == null)
                        {
                            db.Teams.Add(new Team() { TeamID=0, Name="N/A" });
                        }

                        db.Players.Add(new Player() { PlayerID=item.PlayerID,FirstName="N/A" , Name="N/A" , TeamID=0 });
                    }
                }


                //Insertion
                MatchService service = new MatchService(new MatchRepository(pgmContext),new PlayerRepository(pgmContext),null );
                service.replaceScorers(matchDB,scorersAwayTeam);
                
                db.Entry(matchDB).State = System.Data.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    //tres moche mais parfois les buteurs ne sont pas dans la bdd des joueurs et donc ça plante ici
                }
            }

            db.SaveChanges();
        }




        public static List<MatchGoal> ParseAndCreateListMatchGoals(HtmlNode node, int teamID , int matchID)
        {

            List<MatchGoal> res = new List<MatchGoal>();
            
            var allLinks = node.SelectNodes("span/a");
            
            if(allLinks!=null){
                

                //on parcours tous les liens
                foreach (HtmlNode a in allLinks)
                {
                    if (a != null)
                    {


                        string link = a.Attributes["href"].Value;

                        Regex myRegex = new Regex(@"(.*)-(.*)\.html");
                        System.Text.RegularExpressions.Match m = myRegex.Match(link);
                        if (m.Success)
                        {
                            int idJoueur = int.Parse(m.Groups[2].Value);

                            res.Add(new MatchGoal() { PlayerID = idJoueur, TeamID = teamID, MatchID = matchID });

                        }

                    }

                }
            }
            

            return res;
        }

        private static Team GetAndInsertTeam(AppContext db,int id, string url)
        {
            //Check if Team exists, if not we get it with the URL
            Team team = db.Teams.Where(t => t.TeamID == id).FirstOrDefault();

            if(team ==null)
            {
                HtmlWeb web = new HtmlWeb();
                HtmlDocument doc = web.Load("http://www.sports.fr/" + url);

                //Balise h1 contient le titre du club
                HtmlNode h1 = doc.DocumentNode.SelectNodes("//h1").FirstOrDefault();

                string name = h1.InnerText;

                team = new Team() { TeamID = id, Name = name };

                db.Teams.Add(team);
                
                //On va récupérer tous les joueurs du club

                //On récupère le div qui englobe la table des joueurs
                //var r = doc.DocumentNode.SelectNodes("//div[@class='nwTable']");

                HtmlNode divContainer = doc.DocumentNode.SelectNodes("//div[@class='nwTable']")[0];
                int cpt = 0;
                //on parcours tous les tr
                foreach (HtmlNode line in divContainer.SelectNodes("table/tbody/tr"))
                {

                    //On choppe les tds
                    var tds = line.SelectNodes("td");
                    //On prend le 2eme td
                    if (tds != null && tds.Count == 4)
                    {
                        HtmlNode td = tds[1];
                        //On choppe le lien permettant de trouver l'ID du joueur
                        HtmlNode a = td.SelectNodes("a").FirstOrDefault();
                        string link = a.Attributes["href"].Value;

                        Regex myRegex = new Regex(@"(.*)-(.*)\.html");
                        System.Text.RegularExpressions.Match m = myRegex.Match(link);
                        if (m.Success)
                        {
                            int idJoueur = int.Parse(m.Groups[2].Value);

                            //On cherche le nom du joueur
                            string nomJoueur = a.InnerText.Trim();

                            Player player = new Player() { PlayerID = idJoueur, Name = nomJoueur, Team = team };
                            db.Players.Add(player);
                            cpt++;
                        }

                    }

                }

                Console.WriteLine(" -> CLUB "+team.Name + " créé avec "+cpt+" joueurs");

                db.SaveChanges();
                
            }

            return team;

        }


        private static void ParseWebPageLiveScore()
        {


            // http://en.soccerwiki.org/league.php?leagueid=36

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load("http://football-data.enetpulse.com/standings.php?ttFK=53&country=5&oFK=831937&standing=false&round=11");

            //On sélectionne la DIV qui contient le tableau qui nous intéresse
            HtmlNode div = doc.DocumentNode.SelectNodes("//div[@id='LeagueMatches']").FirstOrDefault();

            if (div != null)
            {

                Console.WriteLine("Div " + div.Id + " found");


                //On parcour tous les tr
                foreach (HtmlNode line in div.SelectNodes("table/tr"))
                {
                    Console.WriteLine("tr " + line.Id + " found");

                    //On parcour tous les td

                    var allTd = line.SelectNodes("td");

                    //Si 2 TD c'est une date, on change de date courante pour les matchs
                    if (allTd.Count == 2)
                    {

                    }

                    Console.WriteLine(allTd.Count);



                }


            }
            else
            {
                Console.WriteLine("Div " + div.Id + "NOT found");
            }
            
            



            
        }

      /*  private static void InsertCsvFile()        
        {
             Dictionary<string, Team> Teams = new Dictionary<string, Team>();


            AppContext db = new AppContext();

            //Recherche des teams existantes
            foreach (var item in db.Teams)
            {
                Teams.Add(item.Name, item);
            }




            List<Match> matchs = new List<Match>();

         

            var lines = File.ReadAllLines("F1.csv").Select(a => a.Split(','));
            var csv = from line in lines
                      select (from piece in line
                              select piece);

            int cpt = 0;
            foreach (var line in lines)
            {
                if (cpt > 0) //On zap la 1ere ligne du CSV
                {
                    Match tmp = new Match();

                    tmp.Date = DateTime.ParseExact(line[1], "dd/MM/yy", CultureInfo.InvariantCulture);
                    tmp.Country = "FR";
                    tmp.Div = "F1";

                    string homeTeamName = line[2];
                    string awayTeamName = line[3];


                    //On regarde si pas encore créé
                    if(!Teams.ContainsKey(homeTeamName))
                    {
                        //On créé
                        Team teamTmp = CreateInDb(db,homeTeamName);

                        Teams.Add(teamTmp.Name, teamTmp);
                    }

                    tmp.HomeTeam = Teams[homeTeamName];

                    //On regarde si pas encore créé
                    if (!Teams.ContainsKey(awayTeamName))
                    {
                        //On créé
                        Team teamTmp = CreateInDb(db, awayTeamName);

                        Teams.Add(teamTmp.Name, teamTmp);
                    }

                    tmp.AwayTeam = Teams[awayTeamName];

                    tmp.ScoreHomeTeam = int.Parse(line[4]);
                    tmp.ScoreAwayTeam = int.Parse(line[5]);


                    db.Matchs.Add(tmp);
                    db.SaveChanges();
                }


                

                cpt++;
            }

        }*/


    }
}
