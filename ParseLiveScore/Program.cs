﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseLiveScore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" Parsing Live for " + DateTime.Now);

            bool looping = true;
            


            //On charge tous les matchs du jour
            List<Match> matchsToMonitorToday = LoadMatchOfTheDay();


            while (looping)
            {


            }
            




            Console.WriteLine(" Press any key to quit");
            Console.ReadKey();
        }


        static List<Match> LoadMatchOfTheDay()
        {
            AppContext db = new AppContext();

            return db.Matchs.Where(m => EntityFunctions.TruncateTime(m.Date) == EntityFunctions.TruncateTime(DateTime.Now)).ToList();
        }
    }

    class MatchToParse
    {

        public Match match { get; set; }

        public string url { get; set; }

    }
}
