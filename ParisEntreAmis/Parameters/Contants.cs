﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis
{
    public static class Constants
    {
        /// <summary>
        /// Administration
        /// </summary>
        public const string ROLE_ADMIN = "ADMIN";


        /// <summary>
        /// Jeu
        /// </summary>
        public static string CODE_CATEGORY_SOCCER_FRANCE_L1 = "SOCCER-FR-FOOT-L1";
        public static string CODE_CATEGORY_SOCCER_FRANCE_L2 = "SOCCER-FR-FOOT-L2";

        public static string CODE_CATEGORY_SOCCER_ALLEMAGNE_L1 = "SOCCER-DE-FOOT-L1";

        public static string CODE_CATEGORY_SOCCER_ANGLETERRE_L1 = "SOCCER-EN-FOOT-L1";


        //test
        public static string CODE_CATEGORY_SOCCER_LDC = "SOCCER-FOOT-LDC";

    }
}