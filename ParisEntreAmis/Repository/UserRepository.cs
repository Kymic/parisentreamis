﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class UserRepository : IUserRepository
    {
        //private AppContext context = ContextPerRequest.Current;

        private AppContext context; //= ContextPerRequest.Current;

        public UserRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public UserRepository()
            : this(new WebContext())
        {
        }

        public IQueryable<User> All
        {
            get { return context.Users; }
        }

        public void InsertOrUpdate(User user)
        {
            if (user.UserID == default(int))
            {
                context.Users.Add(user);
            }
            else
            {
                context.Entry(user).State = System.Data.EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

      


        public User GetByUsername(string username)
        {
            return context.Users.FirstOrDefault(u => u.Username == username);
        }


        public string GetUserNameByEmail(string email)
        {
            User user=context.Users.FirstOrDefault(u => u.Email == email);
            return user != null ? user.Username : string.Empty;
        }


        public User GetById(int id)
        {
            return context.Users.FirstOrDefault(u => u.UserID == id);
        }
    }


    public interface IUserRepository 
    {
        IQueryable<User> All { get; }

        User GetByUsername(string username);

        User GetById(int id);

        string GetUserNameByEmail(string email);



        void InsertOrUpdate(User user);
        void Delete(int id);
        void Save();
    }
}