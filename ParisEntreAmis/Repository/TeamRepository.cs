﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class TeamRepository : ITeamRepository
    {

        private AppContext context; //= ContextPerRequest.Current;

        public TeamRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public TeamRepository()
            : this(new WebContext())
        {
        }

        
        public void InsertOrUpdate(Team team)
        {
            if (team.TeamID == default(int) || ! context.Teams.Any(t=>t.TeamID == team.TeamID) )
            {
                context.Teams.Add(team);
            }
            else
            {
                if (context.Entry(team).State == System.Data.EntityState.Detached)
                {
                    context.Teams.Attach(team);
                }
                context.Entry(team).State = System.Data.EntityState.Modified;
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }



        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }


    public interface ITeamRepository : IDisposable
    {
       
        void InsertOrUpdate(Team team);
        void Delete(int id);
        void Save();

    }
}