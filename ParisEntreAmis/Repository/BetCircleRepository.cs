﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class BetCircleRepository : IBetCircleRepository
    {
        //private AppContext context = ContextPerRequest.Current;


        private AppContext context; //= ContextPerRequest.Current;

        public BetCircleRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public BetCircleRepository()
            : this(new WebContext())
        {
        }


        public IQueryable<BetCircle> All
        {
            get { return context.BetCircles; }
        }

        public BetCircle GetById(int id)
        {
            return context.BetCircles.Include("Creator").Where(b=>b.BetCircleID==id).FirstOrDefault();
        }

        public void InsertOrUpdate(BetCircle e)
        {
            if (e.BetCircleID == default(int))
            {
                context.BetCircles.Add(e);
            }
            else
            {
                context.Entry(e).State = System.Data.EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }


        public IQueryable<Bet> GetBetsInCircle(BetCircle c)
        {
            return context.Bets.Include("Match").Include("Match.HomeTeam").Include("Match.AwayTeam").Where(b => b.CircleID == c.BetCircleID);
        }

        public void AddUserToCircle(User userToAdd, BetCircle betCircle)
        {
            if (betCircle.Users == null)
                betCircle.Users = new List<User>();

            betCircle.Users.Add(userToAdd);
        }
        
        public List<User> GetUsersNotInCircle(BetCircle e)
        {
            List<User> res = new List<User>();

            //TODO faire le test coté BDD.
            foreach (var u in context.Users.Where(u=>u.UserID!=e.CreatorID))
            {
                if (!e.Users.Contains(u))
                {
                    res.Add(u);
                }
            }

            return res;

            /*var query = from u in context.Users
                        where u.UserID != e.CreatorID
                        && !(e.Users.Select(c => c.UserID).Contains(u.UserID))
                        select u;*/

            //return query.ToList();
            //return context.Users.Where(u => u.UserID != e.CreatorID &&  !( e.Users.Select(c=>c.UserID).Contains(u.UserID))     ).ToList();
        }

        /// <summary>
        /// Return a circle if the user can access it
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public BetCircle GetById(int id, int userID)
        {
            return this.context.BetCircles.Include("Creator").Where(c => c.BetCircleID == id && (c.Creator.UserID == userID || c.Users.Any(a => a.UserID == userID))).FirstOrDefault();

        }



        public List<User> GetUsersInCircle(int id)
        {

            BetCircle circle = this.GetById(id);

            List<User> res = circle.Users.ToList();
           

            return res;
            
        }


        public List<UserScoreViewModel> GetScoreBoard(int circleID)
        {
            ///
            /// On passe par deux requetes distinctes car c pas simple pr récupérer tous les users meme ceux qui n'ont pas encore joué
            /// TODO à modifier qd tous les users seront dans l'association
            /// avec un left join on pourra tout récupérer.
            ///
            //Récupération de tous les users du cercle
            List<UserScoreViewModel> res = this.GetUsersInCircle(circleID).Select(s=>new UserScoreViewModel() {UserID=s.UserID, Name = s.Username, Score=0}).ToList();

            //Requete qui récupère les scores groupé par userID
            var groupedQuery = this.context.BetPlayeds.Where(b => b.Bet.CircleID == circleID).GroupBy(g => g.UserID).Select(s=> new { userID=s.Key,total=s.Sum(g=>g.Total) });
            
            //On refait le matching
            foreach (var group in groupedQuery)
            {
                var userScore = res.FirstOrDefault(u => u.UserID == group.userID);
                if (userScore != null)
                {
                    userScore.Score = group.total;
                }
            }

            return res;
        }


        public BetCircle GetCircleByGuid(string circleGuid)
        {
            return this.context.BetCircles.FirstOrDefault(c => c.SharedGUID == circleGuid);
        }
    }


    public interface IBetCircleRepository : IDisposable
    {
        IQueryable<BetCircle> All { get; }

        BetCircle GetById(int id);

        BetCircle GetCircleByGuid(string circleGuid);

        IQueryable<Bet> GetBetsInCircle(BetCircle c);

        void InsertOrUpdate(BetCircle e);
        void Delete(int id);
        void Save();

        void AddUserToCircle(User userToAdd, BetCircle betCircle);

        List<User> GetUsersNotInCircle(BetCircle e);

        List<User> GetUsersInCircle(int id);

        List<UserScoreViewModel> GetScoreBoard(int circleID);

        BetCircle GetById(int id, int p);
    }
}