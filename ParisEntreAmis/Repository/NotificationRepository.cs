﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class NotificationRepository : INotificationRepository
    {

        private AppContext context; //= ContextPerRequest.Current;

        public NotificationRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public NotificationRepository()
            : this(new WebContext())
        {
        }

        public Notification InsertOrUpdate(Notification n)
        {
            if (n.NotificationID == default(int))
            {
               n= context.Notifications.Add(n);
            }
            else
            {
                context.Entry(n).State = System.Data.EntityState.Modified;
            }
            return n;

        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }


    public interface INotificationRepository
    {
        Notification InsertOrUpdate(Notification p);
        void Delete(int id);
        void Save();
    }
}