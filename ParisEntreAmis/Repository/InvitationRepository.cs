﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class InvitationRepository : IInvitationRepository
    {

        private AppContext context; //= ContextPerRequest.Current;

        public InvitationRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public InvitationRepository()
            : this(new WebContext())
        {
        }

        public Invitation Insert(Invitation n)
        {            
            return context.Invitations.Add(n);          
        }

        public Invitation Update(Invitation n)
        {   
            context.Entry(n).State = System.Data.EntityState.Modified;
            return n;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }


    public interface IInvitationRepository
    {
        Invitation Insert(Invitation n);
        Invitation Update(Invitation n);

        void Delete(int id);
        void Save();
    }
}