﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class MatchRepository : IMatchRepository
    {

        private AppContext context; //= ContextPerRequest.Current;

        public MatchRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public MatchRepository()
            : this(new WebContext())
        {
        }


        public IQueryable<Match> All
        {
            get { return context.Matchs.Include("HomeTeam").Include("AwayTeam"); }
        }

        public Match GetById(int id)
        {
            return AllFullyLoaded.FirstOrDefault(m => m.MatchID == id);
        }

        public void InsertOrUpdate(Match e)
        {
            if (e.MatchID == default(int))
            {
                context.Matchs.Add(e);
            }
            else
            {                
                context.Entry(e).State = System.Data.EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }


        public void DeleteScorers(Match match)
        {
            if (match.MatchGoals != null)
            {

                foreach (var item in match.MatchGoals.ToList())
                {
                    context.MatchGoals.Remove(item);
                }
            }

        }

        private IEnumerable<Match> AllFullyLoaded
        {
            get
            {
                //return context.Matchs.Include("HomeTeam").Include("AwayTeam").Include("MatchGoals").Include("MatchGoals.Player");
                return context.Matchs;
            }

        }

        public IEnumerable<Match> GetMatchsBeforeDate(DateTime date, int count)
        {
            return AllFullyLoaded.Where(m => m.Date < date ).OrderByDescending(m=>m.Date).Take(count);
        }

        public IEnumerable<Match> GetMatchsAfterDate(DateTime date, int count)
        {
            return AllFullyLoaded.Where(m => m.Date >= date).OrderBy(m => m.Date).Take(count);
        }
    }


    public interface IMatchRepository : IDisposable
    {
        IQueryable<Match> All { get; }

        Match GetById(int id);

        IEnumerable<Match> GetMatchsBeforeDate(DateTime date,int count);

        IEnumerable<Match> GetMatchsAfterDate(DateTime date,int count);

        void InsertOrUpdate(Match match);
        void Delete(int id);
        void Save();

        void DeleteScorers(Match match);
    }
}