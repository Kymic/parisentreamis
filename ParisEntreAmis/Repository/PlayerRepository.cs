﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class PlayerRepository : IPlayerRepository
    {
        //private AppContext context = ContextPerRequest.Current;

        private AppContext context; //= ContextPerRequest.Current;

        public PlayerRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public PlayerRepository()
            : this(new WebContext())
        {
        }

        public IQueryable<Player> All
        {
            get { return context.Players; }
        }

        public Player GetById(int id)
        {
            return context.Players.Find(id);
        }

        public IEnumerable<Player> GetPlayersByClub(Team t)
        {
            return All.Where(p => p.TeamID == t.TeamID);
        }

        public void InsertOrUpdate(Player player)
        {
            if (player.PlayerID == default(int) || !context.Players.Any(p => p.PlayerID == player.PlayerID))
            {
                context.Players.Add(player);
            }
            else
            {
                try
                {

                    if (context.Entry(player).State == System.Data.EntityState.Detached)
                    {
                        context.Players.Attach(player);
                    }
                    context.Entry(player).State = System.Data.EntityState.Modified;


                }
                catch (Exception)
                {
                    //Si ça plante ici, bha tant pis !!
                }

            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }

      
    }


    public interface IPlayerRepository 
    {
        IQueryable<Player> All { get; }

        Player GetById(int id);

        IEnumerable<Player> GetPlayersByClub(Team t);


        void InsertOrUpdate(Player p);
        void Delete(int id);
        void Save();
    }
}