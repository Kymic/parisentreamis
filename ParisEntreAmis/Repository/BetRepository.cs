﻿using ParisEntreAmis.DAL;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Repository
{
    public class BetRepository : IBetRepository
    {
        //private AppContext context = ContextPerRequest.Current;

        private AppContext context; //= ContextPerRequest.Current;

        public BetRepository(IDBContext idbContext)
        {
            context = idbContext.GetContext();
        }

        public BetRepository()
            : this(new WebContext())
        {
        }

        public IQueryable<Bet> All
        {
            get { return context.Bets.Include("Circle"); }
        }

        public IQueryable<Bet> AllFullyLoaded
        {
            get
            {
                return context.Bets.Include("Match").
                      Include("Match.HomeTeam").
                      Include("Match.AwayTeam").
                      Include("Match.MatchGoals").
                      Include("Match.MatchGoals.Player");
            }
        }

        public Bet GetById(int id)
        {
            return context.Bets.Find(id);
        }

        public void InsertOrUpdate(Bet e)
        {
            if (e.BetID == default(int))
            {
                context.Bets.Add(e);
            }
            else
            {
                context.Entry(e).State = System.Data.EntityState.Modified;
            }
        }

        public void Delete(Bet b)
        {
            context.Bets.Remove(b);

        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }


        public List<Bet> GetAllFullBetsFromCircle(BetCircle c)
        {
            return context.Bets.Include("Match").Include("Match.HomeTeam").Include("Match.AwayTeam").Where(b => b.CircleID == c.BetCircleID).OrderBy(m=>m.Match.Date).ToList();
        }

        public Bet GetForBetting(int BetID, User user)
        {

            var query = from b in this.context.Bets
                        
                        where b.BetID == BetID && ( b.Circle.Users.Any(u=>u.UserID==user.UserID))
                        select b;

            return query.FirstOrDefault();
 
        }

        public List<Bet> GetAllFullyLoadedForMatch(Match match)
        {
            return AllFullyLoaded.Where(b => b.MatchID == match.MatchID).ToList();
        }

        public BetPlayed GetBetPlayedByUser(Bet bet, User user)
        {
            return context.BetPlayeds.Where(p => p.BetID == bet.BetID && p.UserID == user.UserID).FirstOrDefault();
        }


        public BetPlayed InsertOrUpdatePlayerBet(BetPlayed betPlayed)
        {
            //On regarde si l'entrée existe déjà.
            BetPlayed existingEntry = context.BetPlayeds.Where(b => b.BetID == betPlayed.BetID && b.UserID == betPlayed.UserID).FirstOrDefault();

            if (existingEntry == null)
            {
                existingEntry = context.BetPlayeds.Add(betPlayed);
            }
            else
            {
                existingEntry.ScoreAwayTeam = betPlayed.ScoreAwayTeam;
                existingEntry.ScoreHomeTeam = betPlayed.ScoreHomeTeam;
                existingEntry.ScorerID = betPlayed.ScorerID;
                existingEntry.UserID = betPlayed.UserID;
                existingEntry.HasNotPlayed = betPlayed.HasNotPlayed;
                context.Entry(existingEntry).State = System.Data.EntityState.Modified;
            }

            context.SaveChanges();

            return existingEntry;
        }


        //TODO REVOIR
        public List<UserBetPlayedViewModel> GetBetPlayedByPlayers(int p)
        {
            return context.Database.SqlQuery<UserBetPlayedViewModel>(@"SELECT u.Username , u.UserID , 
                                                                           b.BetID , b.ScoreHomeTeam , b.ScoreAwayTeam , b.ScorerID ,
                                                                          b.PointsResult ,

                                                                            b.PointsScore ,

                                                                        b.PointsScorer ,

                                                                        b.Penality ,

                                                                        b.IsLooser,

                                                                        b.Total,
                                                                        
                                                                        b.HasNotPlayed,


                                                                           p.Name as ScorerName, p.FirstName as ScorerFirstName
                                                                    FROM BetPlayeds b INNER JOIN Users u ON b.UserID = u.UserID
                                                                            left JOIN  Players p ON b.ScorerID = p.PlayerID WHERE b.BetID = " + p).ToList();


        }






        public void UpdateScoreForPlayers(List<BetPlayed> updateBdd)
        {
            //Update car forcément déjà dans la BDD
            foreach (var item in updateBdd.Where(b=> b.HasNotPlayed==false))
            {
                context.BetPlayeds.Attach(item);

                context.Entry(item).Property(x => x.PointsResult).IsModified = true;
                context.Entry(item).Property(x => x.PointsScore).IsModified = true;
                context.Entry(item).Property(x => x.PointsScorer).IsModified = true;
                context.Entry(item).Property(x => x.Total).IsModified = true;
                context.Entry(item).Property(x => x.IsLooser).IsModified = true;
                context.Entry(item).Property(x => x.HasNotPlayed).IsModified = true;                
            }

            //Update si déjà updaté or Insert si 1ere fois
            foreach (var item in updateBdd.Where(b => b.HasNotPlayed == true))
            {
                var tmp = context.BetPlayeds.FirstOrDefault(b=>b.BetID==item.BetID && b.UserID==item.UserID);
                if (tmp != null)
                {
                    tmp.PointsResult = item.PointsResult;
                    tmp.PointsScore = item.PointsScore;
                    tmp.PointsScorer = item.PointsScorer;
                    tmp.Total = item.Total;
                    tmp.IsLooser = item.IsLooser;
                    tmp.HasNotPlayed = item.HasNotPlayed;
                }
                else
                {
                    context.BetPlayeds.Add(item);
                }

                
            }



            

            context.SaveChanges();

        }
    }


    public interface IBetRepository : IDisposable
    {
        IQueryable<Bet> All { get; }

        IQueryable<Bet> AllFullyLoaded { get; }

        Bet GetById(int id);

        List<Bet> GetAllFullBetsFromCircle(BetCircle c);

        void InsertOrUpdate(Bet bet);
        void Delete(Bet b);
        void Save();

        Bet GetForBetting(int BetID, User user);

        BetPlayed GetBetPlayedByUser(Bet bet, User user);

        BetPlayed InsertOrUpdatePlayerBet(BetPlayed betPlayed);

        List<UserBetPlayedViewModel> GetBetPlayedByPlayers(int p);

        List<Bet> GetAllFullyLoadedForMatch(Match match);

        void UpdateScoreForPlayers(List<BetPlayed> updateBdd);
    }
}