﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class MatchGoal
    {

        public int MatchGoalID { get; set; }


        [ForeignKey("MatchID")]
        public virtual Match Match { get; set; }

        public int MatchID { get; set; }

        [ForeignKey("PlayerID")]
        public virtual Player Player { get; set; }

        public int PlayerID { get; set; }

        public int TeamID { get; set; }

        public int Minute { get; set; }

    }
}