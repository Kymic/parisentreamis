﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class MatchEditModel
    {
        [ReadOnly(true)]
        public Match Match { get; set; }

        public int MatchID { get; set; }

        public int? ScoreHomeTeam { get; set; }

        public int? ScoreAwayTeam { get; set; }

        public string Referee { get; set; }

        public List<Player> PlayersHomeTeam { get; set; }

        public List<Player> PlayersAwayTeam { get; set; }

    }
}