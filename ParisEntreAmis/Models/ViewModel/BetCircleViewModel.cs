﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class BetCircleViewModel
    {

        [ReadOnly(true)]
        public BetCircle BetCircle { get; set; }

        public List<Bet> BetsToDisplay { get; set; }

        public bool IsCurrentUserAdmin { get; set; }

        public bool IsCurrentUserCircleAdmin { get; set; }

        public List<UserScoreViewModel> ScoreBoard { get; set; }

    }
}