﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class UserScoreViewModel
    {
        public int UserID { get; set; }

        public string Name { get; set; }

        public int Score { get; set; }

        public UserBetPlayedViewModel BetPlayedInCurrentContext { get; set; }
    }
}