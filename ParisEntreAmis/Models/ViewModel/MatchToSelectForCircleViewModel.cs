﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class MatchToSelectForCircleViewModel
    {
        public Match Match { get; set; }

        public bool IsAlreadyInCircle { get; set; }

        public Bet Bet { get; set; }

    }
}