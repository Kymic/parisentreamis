﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models.ViewModel
{
    public class UpdateScoresBetweenTwoDatesViewModel
    {
        public DateTime dateStart { get; set; }

        public DateTime dateEnd { get; set; }

        public int NbMatchsUpdated { get; set; }

        public int NbMatchsClosed { get; set; }



    }
}