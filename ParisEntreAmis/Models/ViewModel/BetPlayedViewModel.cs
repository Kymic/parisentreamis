﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class UserBetPlayedViewModel
    {
        public string Username {get; set; }

        public int UserID { get; set; }
        
        public int BetID { get; set; }

        public int? ScoreHomeTeam { get; set; }

        public int? ScoreAwayTeam { get; set; }

        public int? ScorerID { get; set; }

        public string ScorerName { get; set; }

        public string ScorerFirstName { get; set; }

        public int PointsResult { get; set; }

        public int PointsScore { get; set; }

        public int PointsScorer { get; set; }

        public int Penality { get; set; }

        public bool IsLooser { get; set; }

        public int Total { get; set; }

        public bool HasNotPlayed { get; set; }

        public bool DrawGame
        {
            get
            {
                if (ScoreHomeTeam == null || ScoreAwayTeam == null)
                {
                    return false;
                }

                return ScoreHomeTeam.Value == ScoreAwayTeam.Value;
            }
        }

        public bool HomeTeamWon
        {
            get
            {
                if (ScoreHomeTeam == null || ScoreAwayTeam == null)
                {
                    return false;
                }
                return ScoreHomeTeam.Value > ScoreAwayTeam.Value;

            }
        }

        public bool AwayTeamWon
        {
            get
            {
                if (ScoreHomeTeam == null || ScoreAwayTeam == null)
                {
                    return false;
                }
                return ScoreHomeTeam.Value < ScoreAwayTeam.Value;

            }
        }
    }
}