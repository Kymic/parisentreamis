﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class BetsAvailableForCircleEditModel
    {

        public List<MatchToSelectForCircleViewModel> MatchsAvailable { get; set; }

        public BetCircle BetCircle { get; set; }

        public string CategoryName { get; set; }



    }
}