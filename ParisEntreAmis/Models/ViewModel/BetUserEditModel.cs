﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class BetPlayedEditModel
    {

        [ReadOnly(true)]
        public Bet Bet { get; set; }

        [ReadOnly(true)]
        public User User { get; set; }


        public int BetID { get; set; }

        public int? ScoreHomeTeam { get; set; }

        public int? ScoreAwayTeam { get; set; }

        public int? ScorerID { get; set; }

        public Player Scorer { get; set; }

        public List<Player> PlayersHomeTeam { get; set; }

        public List<Player> PlayersAwayTeam { get; set; }

        public BetOddViewModel BetOdds { get; set; }

        public List<UserScoreViewModel> UsersScoreBoardWithBet { get; set; }
        
        public bool CurrentUserDidNotPlayed { get; set; }

        public bool IsAdministrator { get; set; }
    }
}