﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class HomeViewModel : LoginModel
    {

        

        public List<BetCircleViewModel> CirclesJoined { get; set; }


        public List<Match> LastMatchs { get; set; }

        public List<Match> NextMatchs { get; set; }

    }
}