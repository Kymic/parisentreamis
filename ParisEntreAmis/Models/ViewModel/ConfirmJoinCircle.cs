﻿using ParisEntreAmis.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class ConfirmJoinCircle
    {

        [ReadOnly(true)]
        public BetCircle BetCircle { get; set; }

        public CanPeopleJoinCircle Status { get; set; }

    }
}