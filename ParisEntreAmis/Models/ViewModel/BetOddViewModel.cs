﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class BetOddViewModel
    {
        [ReadOnly(true)]
        public Bet Bet { get; set; }

        public List<UserBetPlayedViewModel> BetPlayedByPlayers { get; set; }

        public decimal OddHomeTeamWin { get; set; }

        public decimal OddAwayTeamWin { get; set; }

        public decimal OddDrawGame { get; set; }
    }
}