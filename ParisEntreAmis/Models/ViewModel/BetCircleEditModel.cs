﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class BetCircleEditModel
    {
        [ReadOnly(true)]
        public BetCircle BetCircle { get; set; }


        public string Name { get; set; }

        public List<MatchToSelectForCircleViewModel> MatchsAvailable { get; set; }


        public List<Bet> BetsInCircle { get; set; }

        public List<User> UsersNotInCircle { get; set; }

    }
}