﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class Player
    {
       
        public int PlayerID { get; set; }

        public string Name { get; set; }

        public string FirstName { get; set; }

        [ForeignKey("TeamID")]
        public virtual Team Team { get; set; }


        public int TeamID { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return FirstName+" "+Name;
            }
        }

        public string Url { get; set; }

    }
}