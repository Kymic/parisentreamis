﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{

    /// <summary>
    /// Regroupe un ensemble de paris et de user
    /// 
    /// </summary>
    public class BetCircle
    {
        public int BetCircleID { get; set; }

        public string Name { get; set; }

        public int CreatorID { get; set; }

        public int MaxUser { get; set; }

        public string SharedGUID { get; set; }

        public bool RegistrationOpen { get; set; }

        [ForeignKey("CreatorID")]
        public User Creator { get; set; }

        /// <summary>
        /// Indique si le cercle est éligible au classement global ou non.
        /// Si les matchs sont modifiés le cercle n'est plus éligible
        /// </summary>
        public bool AvailableForGlobalLeader { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }

        public virtual ICollection<User> Users { get; set; }

        
    }
}