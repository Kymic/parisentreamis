﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class Match
    {
        public int MatchID { get; set; }

        public string Country { get; set; }

        public string Div { get; set; }

        public int Day { get; set; }

        public DateTime Date { get; set; }


        public int HomeTeamID { get; set; }

        [ForeignKey("HomeTeamID")]
        public virtual Team HomeTeam { get; set; }

        public int AwayTeamID { get; set; }

        [ForeignKey("AwayTeamID")]
        public virtual Team AwayTeam { get; set; }

        public int? ScoreHomeTeam { get; set; }

        public int? ScoreAwayTeam { get; set; }

        public string Referee { get; set; }

        public virtual List<MatchGoal> MatchGoals { get; set; }
        
        public bool IsLive { get; set; }

        public bool IsFinished{ get; set; }

        public string Url { get; set; }

    }
}