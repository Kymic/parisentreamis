﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class Notification
    {
        public int NotificationID { get; set; }

        public int BetID { get; set; }

        [ForeignKey("BetID")]
        public virtual Bet Bet { get; set; }

        public int UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual User User { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public bool New { get; set; }

        public DateTime Date { get; set; }
    }
}