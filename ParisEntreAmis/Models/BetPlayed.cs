﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class BetPlayed
    {
        [Key, Column(Order=1)]
        public int BetID { get; set; }

        [Key, Column(Order = 2)]
        public int UserID { get; set; }

        [ForeignKey("BetID")]
        public Bet Bet { get; set; }

        [ForeignKey("UserID")]
        public User User { get; set; }

        public int? ScoreHomeTeam { get; set; }

        public int? ScoreAwayTeam { get; set; }

        public int? ScorerID { get; set; }

        public int PointsResult { get; set; }

        public int PointsScore { get; set; }

        public int PointsScorer { get; set; }

        public int Penality { get; set; }

        public bool IsLooser { get; set; }

        public bool HasNotPlayed { get; set; }

        public int Total { get; set; }


        [ForeignKey("ScorerID")]
        public virtual Player Scorer { get; set; }

    }
}