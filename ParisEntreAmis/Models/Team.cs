﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class Team
    {

        public int TeamID { get; set; }

        public string Name { get; set; }

        public string Logo { get; set; }

        public string Url { get; set; }

        public string Country { get; set; }

        public string Div { get; set; }

    }
}