﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class Bet
    {
        public int BetID { get; set; }

        public bool CalculateNormalScore { get; set; }

        public DateTime DeadLine { get; set; }

        public DateTime CreatedOn { get; set; }

        public string Comment { get; set; }

        public int MatchID { get; set; }

        [ForeignKey("MatchID")]
        public virtual Match Match { get; set; }

        public int CircleID { get; set; }

        [ForeignKey("CircleID")]
        public virtual BetCircle Circle { get; set; }

        public virtual User User { get; set; }
        
        [NotMapped]
        public bool IsClosed { get { return DeadLine <= DateTime.Now.AddHours(1); } }
    }
}