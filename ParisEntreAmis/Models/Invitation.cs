﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Models
{
    public class Invitation
    {
        [Key]
        public string InvitationGUID { get; set; }

        public int BetCircleID { get; set; }

        [ForeignKey("BetCircleID")]
        public BetCircle BetCircle { get; set; }

        public DateTime Date { get; set; }

        public bool Active { get; set; }

        public string Message { get; set; }

        public int MaxSubscribtionsNumber { get; set; }

        public int SubscribtionsUsed { get; set; }



    }
}