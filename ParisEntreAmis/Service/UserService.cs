﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Service
{
    public class UserService
    {

        private IUserRepository repository = new UserRepository();

        public User GetCurrentUser()
        {

            HttpContext currentContext = HttpContext.Current;

            if (currentContext.User.Identity.IsAuthenticated)
            {
                return repository.GetByUsername(currentContext.User.Identity.Name);
            }

            return null;
            
        }

        /// <summary>
        /// Retourne la liste des tous les utilisateurs
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll()
        {
            return repository.All.ToList();
        }



        public User GetById(int UserID)
        {
            return repository.GetById(UserID);
        }
    }
}