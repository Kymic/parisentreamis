﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ParisEntreAmis.Service
{
    public class LogConsoleService : ILogService
    {
        public void Info(string log)
        {
            Console.WriteLine("[INFO] "+DateTime.Now.ToLocalTime() + ": " + log);
        }

        public string GetFullLogs()
        {
            throw new NotImplementedException();
        }
    }

    public class LogWebService : ILogService
    {
        private StringBuilder logs;

        

        public void Info(string log)
        {
            if (logs == null)
            {
                logs = new StringBuilder();
            }

            logs.Append(log + System.Environment.NewLine);

        }

        public string GetFullLogs()
        {
            return this.logs != null ? this.logs.ToString() : "";
        }
    }

    public interface ILogService
    {

        string GetFullLogs();

        void Info(string log);

    }
}