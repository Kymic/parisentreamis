﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Service
{
    public class BetService
    {

        private IBetRepository repository;

        private IPlayerRepository playerRepository;

        private IBetCircleRepository circleRepository;

        private NotificationService notificationService;


        public BetService() : this(new BetRepository(), new PlayerRepository(), new BetCircleRepository(), new NotificationService())
        {

        }

        public BetService(IBetRepository _r, IPlayerRepository _p, IBetCircleRepository _circleRepository, NotificationService _n)
        {
            this.repository = _r;
            this.playerRepository = _p;
            this.circleRepository = _circleRepository;
            this.notificationService = _n;
        }

         public Bet SaveChanges(Bet bet)
         {
               this.repository.Save();
               return bet;
         }
        public Bet Create(Match match, BetCircle circle)
        {
            
            Bet bet = new Bet();

            bet.CreatedOn = DateTime.Now;
            bet.Match = match;
            bet.CalculateNormalScore = true;

            //TODO PARAMETRABLE: Deadline du Paris 2 heures avant l'event
            bet.DeadLine = match.Date.AddHours(-2);

            circle.Bets.Add(bet);

            repository.InsertOrUpdate(bet);
            repository.Save();

            return bet;
        }

        public Bet GetForAdministration(int BetID, User user)
        {
            return repository.All.Where(b => b.BetID == BetID && b.Circle.Creator.UserID == user.UserID).FirstOrDefault();
        }

        public Bet GetForBetting(int BetID, User user)
        {
            //Retourne le pari si l'user est dans les membres du cercle associé au pari
            //TODO: attention pour le moment le créateur du cercle n'est pas dans les membres du cercle...
            Bet bet=repository.GetForBetting(BetID, user);

            //...donc on refais un test ici si jamais 
            if(bet==null)
            {
                return GetForAdministration(BetID,user);
            }

            return bet;
        }

        public void Delete(Bet b)
        {
            repository.Delete(b);
            repository.Save();
        }


        public List<Bet> GetAllFullBetsFromCircle(BetCircle c)
        {
            return repository.GetAllFullBetsFromCircle(c);
        }

        public List<Bet> GetAllFullyLoadedForMatch(Match match)
        {
            return repository.GetAllFullyLoadedForMatch(match);
        }

        public BetPlayedEditModel InitBetPlayedEditModel(Bet bet, User user)
        {
            BetPlayedEditModel vm = new BetPlayedEditModel();
            
            vm.PlayersHomeTeam = playerRepository.GetPlayersByClub(bet.Match.HomeTeam).ToList();
            vm.PlayersAwayTeam = playerRepository.GetPlayersByClub(bet.Match.AwayTeam).ToList();

            vm.Bet = bet;
            vm.User = user;
            vm.BetID = bet.BetID;

            //On regarde si un paris a déjà été mis en place.
            BetPlayed betPlayed = repository.GetBetPlayedByUser(bet, user);

            if (betPlayed != null && ! betPlayed.HasNotPlayed )
            {
                vm.ScoreAwayTeam = betPlayed.ScoreAwayTeam;
                vm.ScoreHomeTeam = betPlayed.ScoreHomeTeam;
                vm.Scorer = betPlayed.Scorer;
                vm.ScorerID = betPlayed.ScorerID;

                vm.CurrentUserDidNotPlayed = false;
            }
            else
            {
               vm.CurrentUserDidNotPlayed = true;
            }

            //Si le pari est fermé
            //On obtient le paris des autres joueurs et les côtes
            if (bet.IsClosed)
            {
                vm.BetOdds = CalculateBetOdds(bet);

                //On reconstruit la liste de user avec leur score global 
                //On récupère le leaderboard
                BetCircleService circleService = new BetCircleService();

                vm.UsersScoreBoardWithBet = circleService.GetScoreBoard(vm.Bet.CircleID);

                foreach (var scoreUser in vm.UsersScoreBoardWithBet)
                {
                    //On recherche le paris joué pour l'associé.
                    if(vm.BetOdds.BetPlayedByPlayers !=null )
                    {
                        scoreUser.BetPlayedInCurrentContext = vm.BetOdds.BetPlayedByPlayers.FirstOrDefault(u => u.UserID == scoreUser.UserID);
                    }

                    //Au cas ou
                    if (scoreUser.BetPlayedInCurrentContext == null)
                    {
                        scoreUser.BetPlayedInCurrentContext = new UserBetPlayedViewModel();
                        scoreUser.BetPlayedInCurrentContext.HasNotPlayed = true;
                        scoreUser.BetPlayedInCurrentContext.BetID = vm.BetID;
                    }
                    
                }
                
            }

            //Check si user est admin ou pas
            if (bet.Circle.CreatorID == user.UserID)
            {
                vm.IsAdministrator = true;
            }
            else
            {
                vm.IsAdministrator = false;
            }



            return vm;
        }

        public BetOddViewModel CalculateBetOdds(Bet bet)
        {
            BetOddViewModel vm = new BetOddViewModel();
            vm.Bet = bet;
            vm.BetPlayedByPlayers = repository.GetBetPlayedByPlayers(bet.BetID);

            //Liste de paris rééllement joué
            var betReallyPlayed = vm.BetPlayedByPlayers.Where(b => b.HasNotPlayed == false).ToList();

            //Nombre de paris total rééllement joué
            int nbPlayers = betReallyPlayed.Count;

            if (nbPlayers > 0)
            {
                //On calcul les cotes des paris

                //Nombre de joueurs ayant parié sur HomeTeam
                int nbPlayersHomeTeamWin = betReallyPlayed.Count(b => b.ScoreHomeTeam > b.ScoreAwayTeam);

                //Nombre de joueurs ayant parié sur AwayTeam
                int nbPlayersAwayTeamWin = betReallyPlayed.Count(b => b.ScoreHomeTeam < b.ScoreAwayTeam);

                //Nombre de joueurs ayant parié sur match nul
                int nbPlayersDrawGame = betReallyPlayed.Count(b => b.ScoreHomeTeam == b.ScoreAwayTeam);

                //Si nb joueur = 0, on le force à 1 pour les calculs
                nbPlayersHomeTeamWin = nbPlayersHomeTeamWin == 0 ? 1 : nbPlayersHomeTeamWin;
                nbPlayersAwayTeamWin = nbPlayersAwayTeamWin == 0 ? 1 : nbPlayersAwayTeamWin;
                nbPlayersDrawGame = nbPlayersDrawGame == 0 ? 1 : nbPlayersDrawGame;


               /* vm.OddHomeTeamWin = (decimal)((decimal)1 / (decimal)((decimal)nbPlayersHomeTeamWin / (decimal)nbPlayers));
                vm.OddAwayTeamWin = (decimal)((decimal)1 / (decimal)((decimal)nbPlayersAwayTeamWin / (decimal)nbPlayers));
                vm.OddDrawGame = (decimal)((decimal)1 / (decimal)((decimal)nbPlayersDrawGame / (decimal)nbPlayers));*/
                //Math.Round(rating, MidpointRounding.AwayFromZero)
                
                
                vm.OddHomeTeamWin = Math.Round( ( (decimal) nbPlayers / (decimal) nbPlayersHomeTeamWin)*2 ,MidpointRounding.AwayFromZero )/2 ;                
                vm.OddAwayTeamWin = Math.Round( ( (decimal) nbPlayers / (decimal) nbPlayersAwayTeamWin)*2 ,MidpointRounding.AwayFromZero )/2 ;
                vm.OddDrawGame = Math.Round( ( (decimal) nbPlayers / (decimal) nbPlayersDrawGame)*2 ,MidpointRounding.AwayFromZero )/2 ;

            }
            else
            {
                //Aucun paris, tout est à 0
                vm.OddAwayTeamWin = 0;
                vm.OddDrawGame = 0;
                vm.OddHomeTeamWin = 0;
            }

            return vm;
        }

        /// <summary>
        /// Calcul les scores d'un pari
        /// </summary>
        /// <param name="bet"></param>
        public void ComputeScoreForBet(Bet bet)
        {
            //Si match pas terminé on sort direct
            if (!bet.Match.IsFinished)
            {
                return;
            } 

            //On recupere l'objet avec les cotes
            BetOddViewModel betWithOdds = this.CalculateBetOdds(bet);

            //Données utiles
            //résultats du match
            bool isDrawGame = betWithOdds.Bet.Match.ScoreHomeTeam.Value == betWithOdds.Bet.Match.ScoreAwayTeam.Value;

            bool homeTeamWon = false;
            bool awayTeamWon = false;

            if (!isDrawGame)
            {
                homeTeamWon = betWithOdds.Bet.Match.ScoreHomeTeam.Value > betWithOdds.Bet.Match.ScoreAwayTeam.Value;
                awayTeamWon=betWithOdds.Bet.Match.ScoreHomeTeam.Value < betWithOdds.Bet.Match.ScoreAwayTeam.Value;

            }

            //Ids des buteurs
            List<int> scorersID = betWithOdds.Bet.Match.MatchGoals.Select(s => s.PlayerID).ToList();


            //On construit une liste d'objet pour faire l'update en BDD
            List<BetPlayed> updateBdd = new List<BetPlayed>();

            
            int i=0;

            //Le score le plus faible pour trouver qui a perdu et paye le café (à paramétrer)
            int lowestScore = int.MaxValue;


            //On parcours les paris des joueurs
            foreach (var betPlayed in betWithOdds.BetPlayedByPlayers.Where(b=>b.HasNotPlayed == false))
            {
                 //Sert à faire l'update en BDD
                BetPlayed betUpdateBdd = new BetPlayed();
                betUpdateBdd.BetID = bet.BetID; //Primary KEY
                betUpdateBdd.UserID = betPlayed.UserID; // Primary KEY

                betUpdateBdd.PointsResult=0;
                betUpdateBdd.PointsScore=0;
                betUpdateBdd.PointsScorer=0;



                //Si résultat OK les points sont égaux à deux fois la cote correspondante
                if ( (betPlayed.DrawGame && isDrawGame)
                    || (betPlayed.HomeTeamWon && homeTeamWon)
                    || (betPlayed.AwayTeamWon && awayTeamWon)
                      )
                {
                    //Résultat OK

                    if (bet.CalculateNormalScore)
                    {
                        if (homeTeamWon)
                        {
                            betUpdateBdd.PointsResult = (int)(2 * betWithOdds.OddHomeTeamWin);
                        }
                        else if (awayTeamWon)
                        {
                            betUpdateBdd.PointsResult = (int)(2 * betWithOdds.OddAwayTeamWin);
                        }
                        else if (isDrawGame)
                        {
                            betUpdateBdd.PointsResult = (int)(2 * betWithOdds.OddDrawGame);
                        }
                    }
                    else
                    {
                        //Calcul de points pour les matchs en multiplex
                        betUpdateBdd.PointsResult = 1;
                    }

                }

                if (bet.CalculateNormalScore)
                {
                    //Score OK +3 points
                    if (betPlayed.ScoreHomeTeam.Value == betWithOdds.Bet.Match.ScoreHomeTeam.Value
                            && betPlayed.ScoreAwayTeam.Value == betWithOdds.Bet.Match.ScoreAwayTeam.Value)
                    {
                        betUpdateBdd.PointsScore = 3;
                    }


                    //Buteur NULL et pas de buteur: buteur OK +2 points
                    if (betPlayed.ScorerID == null && scorersID.Count == 0)
                    {
                        betUpdateBdd.PointsScorer = 2;
                    }
                    else if (betPlayed.ScorerID != null && scorersID.Contains(betPlayed.ScorerID.Value))
                    {
                        betUpdateBdd.PointsScorer = 2;
                    }
                }


                betUpdateBdd.Total = betUpdateBdd.PointsResult + betUpdateBdd.PointsScore + betUpdateBdd.PointsScorer;

                if (betUpdateBdd.Total < lowestScore)
                {
                    lowestScore = betUpdateBdd.Total;
                }

                //On reset le flag looser si on recalcul les scores
                betUpdateBdd.IsLooser = false;

                updateBdd.Add(betUpdateBdd);
                
                //On créé une notification 
                CreateNotification(betPlayed.UserID, bet, betUpdateBdd.Total);

                i++;

            }

            //TODO à optimiser
            //On ajoute les joueurs qui n'ont pas parié mais qui font partie du cercle
            foreach (var userNotInCircle in this.circleRepository.GetUsersInCircle(bet.CircleID) )
            {
                //On vérifie que le user n'a pas déjà été ajouté
                if( ! updateBdd.Any(u=>u.UserID == userNotInCircle.UserID))
                {
                    //Sert à faire l'update en BDD
                    BetPlayed betUpdateBdd = new BetPlayed();
                    betUpdateBdd.BetID = bet.BetID; //Primary KEY
                    betUpdateBdd.UserID = userNotInCircle.UserID; // Primary KEY

                    betUpdateBdd.HasNotPlayed = true;//Flag pour dire que le pari n'a pas été joué;
                    betUpdateBdd.PointsResult = 0;
                    betUpdateBdd.PointsScore = 0;
                    betUpdateBdd.PointsScorer = 0;
                    betUpdateBdd.Total = 0;

                    //Le socre le plus bas est forément celui là.
                    lowestScore = 0;

                    //On reset le flag looser si on recalcul les scores
                    betUpdateBdd.IsLooser = false;


                    updateBdd.Add(betUpdateBdd);
                }
            }


            //On cherche le dernier

            var loosers = updateBdd.Where(b => b.Total == lowestScore).ToList();

            //si un seul pas de souci
            if (loosers.Count == 1)
            {
                var looser = loosers.First();
                looser.IsLooser = true;

                CreateNotificationLooser(looser.UserID, bet, looser.Total, false);
            }
            else if(loosers.Count > 1)
            {
                //On fait un random
                Random random = new Random();
                int randomNumber = random.Next(0,loosers.Count);

                var looser = loosers[randomNumber];
                looser.IsLooser = true;

                CreateNotificationLooser(looser.UserID, bet, looser.Total, true);
            }

            repository.UpdateScoreForPlayers(updateBdd);
            
        }

        private void CreateNotificationLooser(int userID, Bet bet,int total, bool random)
        {
            //On créé une notification pour dire qu'on a bien enregistré le paris
            Notification notif = new Notification();
            notif.BetID = bet.BetID;
            notif.UserID = userID;
            notif.Title = "Tu dois payer le café pour la rencontre " + bet.Match.HomeTeam.Name + " - " + bet.Match.AwayTeam.Name;
            if (random)
            {
                notif.Message = "Pas de chance tu as perdu au tirage au sort";
            }
            else
            {
                notif.Message = "Tu as terminé dernier avec: " + total + " points";
            }
            
            notif.New = true;
            notif.Date = DateTime.Now;
            notificationService.Create(notif);
        }

        private void CreateNotification(int userID, Bet bet , int total)
        {
            //On créé une notification pour dire qu'on a bien enregistré le paris
            Notification notif = new Notification();
            notif.BetID = bet.BetID;
            notif.UserID = userID;
            notif.Title = "Résultat du paris de la rencontre " + bet.Match.HomeTeam.Name + " - " + bet.Match.AwayTeam.Name;
            notif.Message = "TOTAL points gagnés: " + total;
            notif.New = true;
            notif.Date = DateTime.Now;
            notificationService.Create(notif);
        }

        
        public void SaveBetPlayed(BetPlayedEditModel BetPlayedEditModel, User user, Bet bet)
        {

            BetPlayed betPlayed = new BetPlayed();
            betPlayed.UserID = user.UserID;
            betPlayed.BetID = BetPlayedEditModel.BetID;
            betPlayed.ScoreAwayTeam = BetPlayedEditModel.ScoreAwayTeam;
            betPlayed.ScoreHomeTeam = BetPlayedEditModel.ScoreHomeTeam;
            betPlayed.ScorerID = BetPlayedEditModel.ScorerID;

            //Si les scores sont différents de null
            //ça signifie que l'utilisateur a joué
            if (betPlayed.ScoreAwayTeam != null && betPlayed.ScoreHomeTeam != null)
            {
                betPlayed.HasNotPlayed = false;
            }

            betPlayed = repository.InsertOrUpdatePlayerBet(betPlayed);

            //On créé une notification pour dire qu'on a bien enregistré le paris
            Notification notif = new Notification();
            notif.BetID = bet.BetID;
            notif.UserID = user.UserID;
            notif.Title = "Pari mis en place pour le match " + bet.Match.HomeTeam.Name + " - " + bet.Match.AwayTeam.Name;
            notif.Message = "Score mis en place: " + BetPlayedEditModel.ScoreHomeTeam + " - " + BetPlayedEditModel.ScoreAwayTeam;
            notif.New = true;
            notif.Date = DateTime.Now;
            notificationService.Create(notif);

        }

        public bool CheckIfUserCanPlay(Bet bet, User user)
        {
            if (bet != null && ! bet.IsClosed )
            {
                return true;
            }

            return false;

        }




    }
}