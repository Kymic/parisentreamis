﻿using HtmlAgilityPack;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Service
{
    public class HtmlParserMatchSportsdotfrService
    {
        
        public List<Match> GetAllMatchsForUrl(string url)
        {

            List<Match> res = new List<Match>();
            
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            
            //On choppe la table avec les résultats

            HtmlNode table = doc.DocumentNode.SelectNodes("//table[@class='nwResultats']").FirstOrDefault();

            //On parcours tous les tr
            foreach (HtmlNode line in table.SelectNodes("tbody/tr"))
            {

                List<MatchGoal> scorersHomeTeam = new List<MatchGoal>();
                List<MatchGoal> scorersAwayTeam = new List<MatchGoal>();


                int? scoreHT = null;
                int? scoreAT = null;

                int homeTeamID=0;
                int awayTeamID=0;

                //On choppe les TD
                var allsTds = line.SelectNodes("td");

                //1er TD correspond à la date du match
                HtmlNode tdDate = allsTds[0];
                string dateStr = tdDate.InnerText;

                //2eme td correspond à l'heure
                HtmlNode tdHour = allsTds[1];
                string hourStr = tdHour.InnerText;

                DateTime dateMatch = DateTime.ParseExact(dateStr + " " + hourStr.Replace("h", ":"), "dd/MM/yy H:m", CultureInfo.InvariantCulture);


                //4eme td correspond à l'équipe 1
                HtmlNode tdHomeTeam = allsTds[3];

                //On choppe le lien permettant de trouver l'ID de l'équipe
                var contenuTd1 = tdHomeTeam.SelectNodes("a");
                if (contenuTd1 == null) //Quand équipe gagnante la balise a est entre balise b
                {
                    contenuTd1 = tdHomeTeam.SelectNodes("b/a");
                }

                HtmlNode a = contenuTd1.FirstOrDefault();


                string link = a.Attributes["href"].Value;

                System.Text.RegularExpressions.Regex myRegex = new System.Text.RegularExpressions.Regex(@"(.*)-(.*)\.html");
                System.Text.RegularExpressions.Match m = myRegex.Match(link);
                if (m.Success)
                {
                    int id = int.Parse(m.Groups[2].Value);
                    homeTeamID = id;
                    //Get the Team or create it 
                    //homeTeam = GetAndInsertTeam(db, id, link);
                }

                //5eme TD correspond au score
                HtmlNode tdScore = allsTds[4];
                string[] score = tdScore.InnerText.Trim().Split('-');
                if (score != null && score.Length == 2)
                {
                    scoreHT = int.Parse(score[0]);
                    scoreAT = int.Parse(score[1]);
                }

                //6eme td correspond à l'équipe 2
                HtmlNode tdAwayTeam = allsTds[5];

                //On choppe le lien permettant de trouver l'ID de l'équipe

                var contenuTd = tdAwayTeam.SelectNodes("a");
                if (contenuTd == null) //Quand équipe gagnante la balise a est entre balise b
                {
                    contenuTd = tdAwayTeam.SelectNodes("b/a");
                }

                HtmlNode a2 = contenuTd.FirstOrDefault();


                string link2 = a2.Attributes["href"].Value;

                System.Text.RegularExpressions.Match m2 = myRegex.Match(link2);
                if (m2.Success)
                {
                    int id = int.Parse(m2.Groups[2].Value);
                    awayTeamID = id;
                    //Get the Team or create it 
                    //awayTeam = GetAndInsertTeam(db, id, link2);
                }


                ParisEntreAmis.Models.Match match = new ParisEntreAmis.Models.Match();
                match.AwayTeamID = awayTeamID;
                match.HomeTeamID = homeTeamID;
                //match.Country = country;
                //match.Div = division;
                match.ScoreAwayTeam = scoreAT;
                match.ScoreHomeTeam = scoreHT;
                match.Date = dateMatch;
                //match.Day = day;
                //match.Url = url;



                //On cherche les buteurs
                //Ils se trouvent dans les balises <span>
                HtmlNode nodeScorersHomeTeam = tdHomeTeam.SelectNodes("small") != null ? tdHomeTeam.SelectNodes("small").FirstOrDefault() : null;

                if (nodeScorersHomeTeam != null) //Si non vide signifie qu'il y a pas de buteurs
                {
                    //On choppe tous les a
                    scorersHomeTeam = ParseAndCreateListMatchGoals(nodeScorersHomeTeam, homeTeamID);
                }

                //On cherche les buteurs
                //Ils se trouvent dans les balises <span>
                HtmlNode nodeScorersAwayTeam = tdAwayTeam.SelectNodes("small") != null ? tdAwayTeam.SelectNodes("small").FirstOrDefault() : null;

                if (nodeScorersAwayTeam != null) //Si non vide signifie qu'il y a pas de buteurs
                {
                    //On choppe tous les a
                    scorersAwayTeam = ParseAndCreateListMatchGoals(nodeScorersAwayTeam, awayTeamID);
                }

                scorersAwayTeam.AddRange(scorersHomeTeam);


                match.MatchGoals = scorersAwayTeam;
                res.Add(match);
            }



            return res;

        }

        private List<MatchGoal> ParseAndCreateListMatchGoals(HtmlNode node, int teamID)
        {
            List<MatchGoal> res = new List<MatchGoal>();

            var allLinks = node.SelectNodes("span/a");

            if (allLinks != null)
            {


                //on parcours tous les liens
                foreach (HtmlNode a in allLinks)
                {
                    if (a != null)
                    {
                        string link = a.Attributes["href"].Value;

                        System.Text.RegularExpressions.Regex myRegex = new System.Text.RegularExpressions.Regex(@"(.*)-(.*)\.html");
                        System.Text.RegularExpressions.Match m = myRegex.Match(link);
                        if (m.Success)
                        {
                            int idJoueur = int.Parse(m.Groups[2].Value);

                            res.Add(new MatchGoal() { PlayerID = idJoueur, TeamID = teamID });

                        }

                    }

                }
            }


            return res;
        }


    }
}