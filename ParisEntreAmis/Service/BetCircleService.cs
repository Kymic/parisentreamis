﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Service
{
    public enum CanPeopleJoinCircle
    {
        Yes,
        LimitReach,
        AlreadyMember,
        InvitationDisabled
    }

    public class BetCircleService
    {

        private IBetCircleRepository repository;
        private IMatchRepository matchRepository;

        private BetService betService = new BetService();

        private UserService userService = new UserService();


        public BetCircleService() : this(new BetCircleRepository(), new MatchRepository())
        {

        }

        public BetCircleService(IBetCircleRepository _r, IMatchRepository _m)
        {
            this.repository = _r;
            this.matchRepository = _m;
        }

        public void Create(BetCircle e, User u)
        {
            e.Creator = u;

            e.MaxUser = 25;
            e.RegistrationOpen = true;
            e.SharedGUID = Guid.NewGuid().ToString();

            e.AvailableForGlobalLeader = false;

            repository.InsertOrUpdate(e);
            repository.Save();

            repository.AddUserToCircle(u, e);
            repository.Save();

        }

        public void Update(BetCircle e)
        {
            repository.InsertOrUpdate(e);
            repository.Save();
        }

       /* public Bet AddBet(BetCircle circle, Match match)
        {
            //Bet
        }*/

        public List<BetCircle> GetAll()
        {
            return repository.All.ToList();
        }


        public List<BetCircle> GetCirclesAdministredByUser(User user)
        {
            return repository.All.Where(c => c.Creator.UserID == user.UserID).ToList();
        }

        public List<BetCircleViewModel> GetCirclesForUser(User user)
        {
            List<BetCircleViewModel> res = new List<BetCircleViewModel>();

            foreach (var item in repository.All.Where(c => c.Creator.UserID == user.UserID || c.Users.Any(a => a.UserID == user.UserID)))
            {
                res.Add(new BetCircleViewModel() { BetCircle=item, IsCurrentUserAdmin = (item.CreatorID==user.UserID) });
            }

            return res;
        }


        public BetCircle GetForAdministration(int id, User usr)
        {
            return repository.All.Where(c => c.BetCircleID == id && c.Creator.UserID == usr.UserID).FirstOrDefault();
        }

        public BetCircleEditModel InitEditModel(BetCircle e)
        {
            BetCircleEditModel em = new BetCircleEditModel();
            em.BetCircle = e;
            em.Name = e.Name;

            em.BetsInCircle = repository.GetBetsInCircle(e).ToList();

            em.UsersNotInCircle = this.repository.GetUsersNotInCircle(e);

            //em.MatchsAvailable = this.matchRepository.All.Where(m => m.Country == "FR" && m.Div == "Ligue 1").ToList();

            return em;
        }

        public BetsAvailableForCircleEditModel GetAvailableBetsInCategory(BetCircle c, string CodeCategory)
        {
            BetsAvailableForCircleEditModel res = new BetsAvailableForCircleEditModel();
            res.BetCircle = c;
            res.MatchsAvailable = new List<MatchToSelectForCircleViewModel>();

            IQueryable<Match> queryMatchs = null;

            //TODO à revoir
            if(CodeCategory.Equals(Constants.CODE_CATEGORY_SOCCER_FRANCE_L1))
            {
                res.CategoryName = "France: Ligue 1 ";
                queryMatchs = this.matchRepository.All.Where(m => m.Country == "FR" && m.Div == "Ligue 1" && m.Date >= DateTime.Now);
            }
            else if (CodeCategory.Equals(Constants.CODE_CATEGORY_SOCCER_FRANCE_L2))
            {
                res.CategoryName = "France: Ligue 2 ";
                queryMatchs = this.matchRepository.All.Where(m => m.Country == "FR" && m.Div == "Ligue 2" && m.Date >= DateTime.Now);
            }
            else if (CodeCategory.Equals(Constants.CODE_CATEGORY_SOCCER_ALLEMAGNE_L1))
            {
                res.CategoryName = "Allemagne: Bundesliga";
                queryMatchs = this.matchRepository.All.Where(m => m.Country == "DE" && m.Div == "Ligue 1" && m.Date >= DateTime.Now);
            }
            else if (CodeCategory.Equals(Constants.CODE_CATEGORY_SOCCER_ANGLETERRE_L1))
            {
                res.CategoryName = "Angleterre: Premier League";
                queryMatchs = this.matchRepository.All.Where(m => m.Country == "EN" && m.Div == "Ligue 1" && m.Date >= DateTime.Now);
            }
            else if (CodeCategory.Equals(Constants.CODE_CATEGORY_SOCCER_LDC))
            {
                res.CategoryName = "Ligue des Champions";
                queryMatchs = this.matchRepository.All.Where(m => m.Country == "LDC" && m.Div == "LDC" && m.Date >= DateTime.Now);
            }


            //On regarde les matchs déjà ajoutés au cercle

            List<Match> matchsAlreadyInCircle = c.Bets.Select(m => m.Match).ToList();

            foreach (var item in queryMatchs.OrderBy(m=>m.Date) )
            {
                MatchToSelectForCircleViewModel m = new MatchToSelectForCircleViewModel();
                m.Match = item;

                if(matchsAlreadyInCircle.Contains(item))
                {
                    m.IsAlreadyInCircle=true;
                }
                else
                {
                    m.IsAlreadyInCircle=false;
                }

                res.MatchsAvailable.Add(m);
                
            }
            
            return res;

        }

        public void RemoveBetFromCircle(Bet b, BetCircle c)
        {
            c.Bets.Remove(b);

            betService.Delete(b);

        }

        /// <summary>
        /// Supprime le paris correspondant au match passé en paramètre du cercle passé en paramètre
        /// </summary>
        /// <param name="match"></param>
        /// <param name="c"></param>
        /// <returns>return l'ID du paris supprimé</returns>
        public int RemoveMatchFromCircle(Match match, BetCircle c)
        {
            //On va récupérer le paris correspondant au match dans le cercle

            Bet bet = c.Bets.Where(m => m.MatchID == match.MatchID).FirstOrDefault();

            if (bet != null)
            {
                RemoveBetFromCircle(bet, c);
            }

            return bet.BetID;
        }


        public void AddUserToCircle(int UserID, BetCircle betCircle)
        {
            //On check si UserID est un vrai user
            User userToAdd = userService.GetById(UserID);

            if (userToAdd == null)
            {
                throw new Exception("bad user");
            }

            if(betCircle.CreatorID== userToAdd.UserID || betCircle.Users.Any(u=>u.UserID==userToAdd.UserID) )
            {
                throw new Exception("User is already in circle");
            }

            repository.AddUserToCircle(userToAdd , betCircle);
            repository.Save();
        }

        public BetCircle Get(int id, User usr)
        {

            return repository.GetById(id, usr.UserID);
        }

        public BetCircleViewModel InitBetCircleViewModel(BetCircle c, User user)
        {
            BetCircleViewModel vm = new BetCircleViewModel();
            vm.BetCircle = c;
            vm.BetsToDisplay = betService.GetAllFullBetsFromCircle(c);

            vm.IsCurrentUserCircleAdmin = c.CreatorID == user.UserID;

            vm.ScoreBoard= this.GetScoreBoard(c.BetCircleID);

            return vm;
        }


        public List<UserScoreViewModel> GetScoreBoard(int circleID)
        {
            List<UserScoreViewModel> res = new List<UserScoreViewModel>();

            res = this.repository.GetScoreBoard(circleID).OrderByDescending(u=>u.Score).ToList();

            return res;
        }

        public BetCircle GetCircleByGuid(string circleGuid)
        {
            return this.repository.GetCircleByGuid(circleGuid);
        }

        public CanPeopleJoinCircle DoesUserCanJoinCircle(BetCircle circle, User user)
        {
            //On vérifie
            if (circle.Users.Contains(user))
            {
                return CanPeopleJoinCircle.AlreadyMember;
            }
            
            if (circle.RegistrationOpen == false)
            {
                return CanPeopleJoinCircle.InvitationDisabled;
            }

            if (circle.Users.Count == circle.MaxUser)
            {
                return CanPeopleJoinCircle.LimitReach;
            }

            return CanPeopleJoinCircle.Yes;

        }

    }
}