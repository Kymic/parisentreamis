﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using ParisEntreAmis.Models.ViewModel;


namespace ParisEntreAmis.Service
{
    public class UpdateMatchService
    {

        private IMatchRepository matchRepository;

        private MatchService matchService;

        public UpdateMatchService() : this(new MatchRepository() )
        {

        }

        public UpdateMatchService(IMatchRepository _r)
        {
            this.matchRepository = _r;

            matchService = new MatchService();
        }


        /// <summary>
        /// Mets à jours tous les matchs se déroulant entre deux dates n'ayant pas encore été traités
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public UpdateScoresBetweenTwoDatesViewModel UpdateScoresBetweenTwoDates(DateTime start, DateTime end)
        {

            UpdateScoresBetweenTwoDatesViewModel vm = new UpdateScoresBetweenTwoDatesViewModel();

            vm.dateEnd = end;
            vm.dateStart = start;
            vm.NbMatchsClosed = 0;
            vm.NbMatchsUpdated = 0;

            HtmlParserMatchSportsdotfrService parser = new HtmlParserMatchSportsdotfrService();
            
            //On récupère une liste d'URL à parser
            /*List<string> urlsToParse = this.matchRepository.All.Where(m => m.IsFinished == false && m.Date >= start && m.Date <= end).
                                    GroupBy(m => m.Url).Select(g=> g.Key).ToList();*/
            List<string> urlsToParse = this.matchRepository.All.Where(m => m.IsFinished == false &&  m.Date <= end).
                                    GroupBy(m => m.Url).Select(g => g.Key).ToList();

            foreach (var url in urlsToParse)
            {
                if (!string.IsNullOrEmpty(url))
                {
                    
                    //On récupère une liste de match grâce à l'URL
                    List<Match> matchsToUpdate = parser.GetAllMatchsForUrl(url);

                    //Dans cette liste on filtre les matchs également par les dates
                    //matchsToUpdate = matchsToUpdate.Where(m => m.IsFinished == false && m.Date >= start && m.Date <= end).ToList();
                    matchsToUpdate = matchsToUpdate.Where(m => m.Date <= end).ToList();

                    //On va recuperer en BDD ces matchs pour les updater.

                    foreach (var matchToUpdate in matchsToUpdate)
                    {
                        Match matchDB = matchRepository.All.Where(t => EntityFunctions.TruncateTime(t.Date) == EntityFunctions.TruncateTime(matchToUpdate.Date) && t.AwayTeam.TeamID == matchToUpdate.AwayTeamID && t.HomeTeam.TeamID == matchToUpdate.HomeTeamID).FirstOrDefault();

                        //Si match dejà flagé comme terminé en BDD on break;
                        if (matchDB != null && !matchDB.IsFinished)
                        {

                            matchDB.ScoreAwayTeam = matchToUpdate.ScoreAwayTeam;
                            matchDB.ScoreHomeTeam = matchToUpdate.ScoreHomeTeam;
                            matchDB.Date = matchToUpdate.Date;

                            //On update la liste des buteurs avec l'ID du match
                            foreach (var scorer in matchToUpdate.MatchGoals)
                            {
                                scorer.MatchID = matchDB.MatchID;
                            }

                            matchService.replaceScorers(matchDB, matchToUpdate.MatchGoals);

                            vm.NbMatchsUpdated++;

                            //On check si on doit changer les status du match
                            if (matchService.CanWeChangeIsFinishedToYes(matchDB))
                            {
                                matchDB.IsFinished = true;
                                matchDB.IsLive = false;

                                vm.NbMatchsClosed++;

                                //Calcul des scores des paris associés à ce match
                                matchService.ComputeBetsForMatch(matchDB);
                            }

                            matchService.saveChanges(matchDB);
                        }
                    }
                }
            }

            return vm;

        }


    }
}