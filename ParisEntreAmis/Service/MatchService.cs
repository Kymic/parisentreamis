﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Service
{
    public class MatchService
    {

        private IMatchRepository repository;
        private IPlayerRepository playerRepository;

        private BetService betService;

        public MatchService() : this(new MatchRepository(), new PlayerRepository() , new BetService() )
        {

        }

        public MatchService(IMatchRepository _r, IPlayerRepository _p , BetService _b)
        {
            this.repository = _r;
            this.playerRepository = _p;

            this.betService = _b;

        }

        public Match GetById(int id)
        {
            return repository.GetById(id);
        }

        public MatchEditModel InitMatchEditModel(Match match)
        {
            MatchEditModel em = new MatchEditModel();

            em.Match = match;
            em.MatchID = match.MatchID;
            em.Referee = match.Referee;
            em.ScoreAwayTeam = match.ScoreAwayTeam;
            em.ScoreHomeTeam = match.ScoreHomeTeam;
            
            
            em.PlayersHomeTeam = playerRepository.GetPlayersByClub(match.HomeTeam).ToList();
            em.PlayersAwayTeam = playerRepository.GetPlayersByClub(match.AwayTeam).ToList();
            
            return em;
        }



        public Match saveChanges(Match match)
        {
            repository.InsertOrUpdate(match);
            repository.Save();
            return match;

        }

        public void replaceScorers(Match match, List<MatchGoal> scorers)
        {

            repository.DeleteScorers(match);
            
            match.MatchGoals = scorers;

            //this.saveChanges(match);

        }

        public void ComputeBetsForMatch(Match match)
        {
            //On récupère les bets associé au match

            foreach (var bet in betService.GetAllFullyLoadedForMatch(match) )
	        {
                betService.ComputeScoreForBet(bet);
	        }            

        }

        public List<Match> GetMatchsBeforeDate(DateTime date,int count=5)
        {
            return repository.GetMatchsBeforeDate(date.AddMinutes(-100),count).ToList();
        }

        public List<Match> GetMatchsAfterDate(DateTime date,int count=5)
        {
            return repository.GetMatchsAfterDate(date.AddMinutes(-100),count).ToList();
        }

        /// <summary>
        /// Si le score du match est différent de null et que le match est passé de deux heures on considère qu'il est terminé
        /// </summary>
        /// <param name="matchDB"></param>
        /// <returns></returns>
        public bool CanWeChangeIsFinishedToYes(Match matchDB)
        {
            if (matchDB.ScoreAwayTeam != null && matchDB.ScoreHomeTeam != null)
            {

                //Si score différent de 0 on regarde si on a récupéré les buteurs
                //parfois sur sports.fr les buteurs sont mis à jour plus tard
                if (matchDB.ScoreAwayTeam.Value != 0 || matchDB.ScoreHomeTeam.Value != 0)
                {
                    //On doit avoir des buteurs
                    if (matchDB.MatchGoals==null || matchDB.MatchGoals.Count == 0)
                    {
                        return false;
                    }
                }
                
                    return matchDB.Date <= DateTime.Now.AddHours(-2);
                                
            }

            return false;
        }
    }
}