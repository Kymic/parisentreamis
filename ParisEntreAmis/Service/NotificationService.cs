﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.Service
{


    public class NotificationService
    {

        private INotificationRepository notificationRepository;



        public NotificationService() : this(new NotificationRepository())
        {

        }

        public NotificationService(INotificationRepository _r)
        {
            this.notificationRepository = _r;         
        }

        public Notification Create(Notification n)
        {

            n.New = true;
            n=this.notificationRepository.InsertOrUpdate(n);

            this.notificationRepository.Save();


            FirebaseService fireBaseService = new FirebaseService();

            fireBaseService.PushNotification(n);

            return n;
        }

        public void Create(string title, string message , Bet bet , List<User> users)
        {
            foreach (var user in users)
            {
                Notification n = new Notification();
                n.New = true;
                n.Title = title;
                n.Message = message;
                n.UserID = user.UserID;
                n.BetID = bet.BetID;

                this.notificationRepository.InsertOrUpdate(n);
            }

            this.notificationRepository.Save();
        }



    }
}