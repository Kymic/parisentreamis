﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using ParisEntreAmis.Models.ViewModel;


namespace ParisEntreAmis.Service
{
    public class UpdateTeamService
    {

        private ITeamRepository teamRepository;
        private IPlayerRepository playerRepository;


        private ILogService logger;


        public UpdateTeamService() : this(new TeamRepository(), new PlayerRepository() , new LogWebService() )
        {

        }

        public UpdateTeamService(ITeamRepository _t, IPlayerRepository _r, ILogService _l)
        {
            this.teamRepository = _t;
            this.playerRepository = _r;
            this.logger = _l;

        }

        public string GetFullLogs()
        {
            return this.logger.GetFullLogs();
        }
        
        public List<Team> UpdateTeamsForCountryAndCategory(string country, string category , string url)
        {
            HtmlParserTeamSportsdotfrService parser = new HtmlParserTeamSportsdotfrService(this.logger);

            List<Team> res = parser.GetAllTeamsFromUrl(url);


            //On parcours la liste obtenu afin de faire les update ou les insert en BDD

            foreach (var team in res)
            {
                team.Country = country;
                team.Div = category;

                teamRepository.InsertOrUpdate(team);

                this.logger.Info("Club " + team.Name + " sauvegardé");


            }
            
            teamRepository.Save();
            this.logger.Info( res.Count+ " clubs ont été sauvegardé en BDD");
            this.logger.Info(" ");


            //On reparcours la liste pour parser et insérer ou updater les joueurs
            foreach (var team in res)
            {
                List<Player> players = parser.GetAllPlayersForTeamFromUrl(team.Url, team);
                int cpt = 0;
                foreach (var player in players)
                {
                    playerRepository.InsertOrUpdate(player);
                    cpt++;
                }
                playerRepository.Save();
                this.logger.Info(cpt+ " joueurs sauvegardés en BDD pour "+team.Name);

            }


            return res;
        }


       


    }
}