﻿using HtmlAgilityPack;
using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ParisEntreAmis.Service
{
    public class HtmlParserTeamSportsdotfrService
    {
        private ILogService logService;

        public HtmlParserTeamSportsdotfrService(ILogService logService)
        {
            // TODO: Complete member initialization
            this.logService = logService;
        }


        //URL : http://www.sports.fr/football/ligue-1/clubs.html

        public List<Team> GetAllTeamsFromUrl(string url)
        {
            List<Team> res = new List<Team>();



            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);

            //On choppe le div qui contient la liste des clubs

            HtmlNode div = doc.DocumentNode.SelectNodes("//div[@class='nwTable nwClub']").FirstOrDefault();
            
            //On parcours tous les li
            foreach (HtmlNode line in div.SelectNodes("ul/li"))
            {
                //on choppe le h2
                HtmlNode link = line.SelectSingleNode("div/h2/a");

                string urlTeam = link.Attributes["href"].Value;
                
                Regex myRegex = new Regex(@"(.*)-(.*)\.html");
                System.Text.RegularExpressions.Match m = myRegex.Match(urlTeam);
                if (m.Success)
                {
                    int id = int.Parse(m.Groups[2].Value);
                    
                    Team t = new Team();
                    
                    t.TeamID = id;
                    t.Name = link.InnerText.Trim();
                    t.Url = "http://www.sports.fr"+urlTeam;
                    
                    res.Add(t);
                    this.logService.Info(t.Name + " a été correctement parsé");
                }
            }

            return res;
        }




        public List<Player> GetAllPlayersForTeamFromUrl(string url, Team team)
        {
            List<Player> res = new List<Player>();
            

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);

            HtmlNode divContainer = doc.DocumentNode.SelectNodes("//div[@class='nwTable']")[0];
            
            //on parcours tous les tr
            foreach (HtmlNode line in divContainer.SelectNodes("table/tbody/tr"))
            {

                //On choppe les tds
                var tds = line.SelectNodes("td");
                //On prend le 2eme td
                if (tds != null && tds.Count == 4)
                {
                    HtmlNode td = tds[1];
                    //On choppe le lien permettant de trouver l'ID du joueur
                    HtmlNode a = td.SelectNodes("a").FirstOrDefault();
                    string link = a.Attributes["href"].Value;

                    Regex myRegex = new Regex(@"(.*)-(.*)\.html");
                    System.Text.RegularExpressions.Match m = myRegex.Match(link);
                    if (m.Success)
                    {
                        int idJoueur = int.Parse(m.Groups[2].Value);

                        //On cherche le nom du joueur
                        string nomJoueur = a.InnerText.Trim();

                        Player player = new Player() { PlayerID = idJoueur, Name = nomJoueur , Team = team, TeamID = team.TeamID};
                        res.Add(player);

                    }

                }

            }

            return res;

        }



    }
}