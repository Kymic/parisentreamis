﻿using ParisEntreAmis.Models.ViewModel;
using ParisEntreAmis.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParisEntreAmis.Controllers
{
    public class UpdateMatchController : Controller
    {

        private UpdateMatchService updaterMatchService = new UpdateMatchService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UpdateAllMatch()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            UpdateScoresBetweenTwoDatesViewModel vm = updaterMatchService.UpdateScoresBetweenTwoDates(DateTime.Now.AddHours(-2), DateTime.Now);

            sw.Stop();
            ViewBag.executionTime = sw.ElapsedMilliseconds;

            return View(vm);
        }

    }
}
