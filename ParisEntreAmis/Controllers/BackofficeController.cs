﻿using ParisEntreAmis.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParisEntreAmis.Controllers
{
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public class BackofficeController : BaseController
    {



        private UserService userService = new UserService();


        //
        // GET: /Backoffice/

        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Liste les utilisateurs
        /// </summary>
        /// <returns></returns>
        public ActionResult Users()
        {
            return View(userService.GetAll());
        }
        
        public ActionResult Spam1234()
        {
            var smtpClient = new System.Net.Mail.SmtpClient();
            smtpClient.Send(new System.Net.Mail.MailMessage("vincent.durey@kuhn.com","durey.vincent@gmail.com","yoo","yoo"));
            return View(userService.GetAll());
        }

    }
}
