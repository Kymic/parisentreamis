﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using ParisEntreAmis.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParisEntreAmis.Controllers
{
    [Authorize]
    public class BetCircleController : BaseController
    {

        private BetCircleService circleService = new BetCircleService();
        private BetService betService = new BetService();
        private MatchService matchService = new MatchService();


        //
        // GET: /BetCircle/

        public ActionResult Index()
        {
            
            return View(circleService.GetAll());
        }



        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(BetCircle b)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    User usr =null;

                    var repo = new UserRepository();
                   
                    usr  = repo.GetByUsername(User.Identity.Name);
                    

                    if (usr == null)
                        throw new Exception("Bad user");

                    circleService.Create(b, usr);
                    return RedirectToAction("Edit", new { id = b.BetCircleID });
                }
                catch (Exception ex)
                {

                    ModelState.AddModelError("",ex.Message);

                }
            }


            return View(b);
        }

        public ActionResult Details(int id) 
        {
            User usr = null;

            var repo = new UserRepository();
           
            usr = repo.GetByUsername(User.Identity.Name);
           

            if (usr == null)
                throw new Exception("Bad user");

            BetCircle betSet = this.circleService.Get(id,usr);

            if (betSet == null)
            {
                return HttpNotFound();
            }


            var tokenGenerator = new Firebase.TokenGenerator("3BNGwIhvAPAfIJIZLANvdsiwgOuAR1lfQtnYORaE");
            var authPayload = new Dictionary<string, object>()
            {                
                { "Username" , usr.Username },
                { "circleID" , betSet.BetCircleID .ToString()}
            };

            string token = tokenGenerator.CreateToken(authPayload);

            ViewBag.firebaseToken = token;

            return View(this.circleService.InitBetCircleViewModel(betSet, usr));

        }


        public ActionResult Edit(int id)
        {
            User usr = null;

            var repo = new UserRepository();

            usr = repo.GetByUsername(User.Identity.Name);


            if (usr == null)
                throw new Exception("Bad user");

            BetCircle betSet = this.circleService.GetForAdministration(id, usr);

            if (betSet == null)
            {
                return HttpNotFound();
            }

            return View(this.circleService.InitEditModel(betSet));

        }

        public ActionResult DisplayBetsAvailableInCategory(int BetCircleID , string CodeCategory)
        {
            User usr = null;

            var repo = new UserRepository();

            usr = repo.GetByUsername(User.Identity.Name);

            BetCircle circle = this.circleService.GetForAdministration(BetCircleID, usr);
            if (circle == null)
            {
                return HttpNotFound();
            }


            BetsAvailableForCircleEditModel res = this.circleService.GetAvailableBetsInCategory(circle, CodeCategory);

            return PartialView("_AddBetToCircle", res);

        }


        //
        // GET: /Bet/

        public ActionResult AddMatchToCircle(int MatchID, int CircleID)
        {


            User user = new UserService().GetCurrentUser();

            if (user == null)
            {
                return HttpNotFound();
            }


            BetCircle betSet = circleService.GetForAdministration(CircleID, user);

            if (betSet == null)
            {
                return HttpNotFound();
            }


            Match match = matchService.GetById(MatchID);

            if (match == null)
            {
                return HttpNotFound();
            }

            Bet bet = betService.Create(match, betSet);

            return PartialView("_EditBetsInCircle", bet);
        }


        public ActionResult RemoveMatchFromCircle(int MatchID, int CircleID)
        {

            User user = new UserService().GetCurrentUser();

            if (user == null)
            {
                return HttpNotFound();
            }

            BetCircle betSet = circleService.GetForAdministration(CircleID, user);

            if (betSet == null)
            {
                return HttpNotFound();
            }

            Match match = matchService.GetById(MatchID);

            if (match == null)
            {
                return HttpNotFound();
            }


            int betIdDeleted = circleService.RemoveMatchFromCircle(match, betSet);


            return Json(betIdDeleted, JsonRequestBehavior.AllowGet);
        }


        public ActionResult RemoveBetFromCircle(int BetID, int MatchID)
        {
            User user = new UserService().GetCurrentUser();

            if (user == null)
            {
                return HttpNotFound();
            }

            Bet bet = betService.GetForAdministration(BetID, user);

            if (bet == null)
            {
                return HttpNotFound();
            }

            Match match = matchService.GetById(MatchID);

            if (match == null)
            {
                return HttpNotFound();
            }

            betService.Delete(bet);

            return Json(0, JsonRequestBehavior.AllowGet);
        }


        public ActionResult JoinCircle(string circleGuid)
        {
            User user = new UserService().GetCurrentUser();

            ConfirmJoinCircle vm = new ConfirmJoinCircle();

            BetCircle circle = this.circleService.GetCircleByGuid(circleGuid);

            if (circle == null)
            {
                return HttpNotFound();
            }

            vm.BetCircle = circle;
            vm.Status = this.circleService.DoesUserCanJoinCircle(circle, user);


            return View(vm);
        }

        public ActionResult ConfirmJoinCircle(string circleGuid)
        {
            
            User user = new UserService().GetCurrentUser();

            ConfirmJoinCircle vm = new ConfirmJoinCircle();

            BetCircle circle = this.circleService.GetCircleByGuid(circleGuid);

            if (circle == null)
            {
                return HttpNotFound();
            }

            vm.BetCircle = circle;
            vm.Status = this.circleService.DoesUserCanJoinCircle(circle, user);


            if (vm.Status == CanPeopleJoinCircle.Yes)
            {
                //on ajoute le user et on le redirige vers le cercle
                this.circleService.AddUserToCircle(user.UserID, circle);

                return RedirectToAction("Details", new { id = circle.BetCircleID });
            }


            //si on est là on réaffiche la la page pour expliquer pkoi le user ne peut pas rejoindre le cercle
            return View("JoinCircle", vm);
        }


        /*public ActionResult AddUserToCircle(int UserID, int CircleID)
        {
            User user = new UserService().GetCurrentUser();

            if (user == null)
            {
                return HttpNotFound();
            }

            BetCircle betSet = circleService.GetForAdministration(CircleID, user);


            circleService.AddUserToCircle(UserID, betSet);


            return Json(user.UserID);
        }*/

    
    }
}
