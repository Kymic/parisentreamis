﻿using ParisEntreAmis.Filters;
using ParisEntreAmis.Models;
using ParisEntreAmis.Repository;
using ParisEntreAmis.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace ParisEntreAmis.Controllers
{
    public class HomeController : BaseController
    {
        private BetCircleService service = new BetCircleService();

        private MatchService matchService = new MatchService();

        public ActionResult Index()
        {

            
            
            //var u = repo.All.FirstOrDefault();
            

            HomeViewModel vm = new HomeViewModel();
            if (User.Identity.IsAuthenticated)
            {
                var repo = new UserRepository();    
                User usr = repo.GetByUsername(User.Identity.Name);
               
                if (usr == null)
                {
                    return RedirectToAction("Login", "Account");

                   // return HttpNotFound();
                }

                vm.CirclesJoined = service.GetCirclesForUser(usr);

            }


            vm.LastMatchs = matchService.GetMatchsBeforeDate(DateTime.Now);

            vm.NextMatchs = matchService.GetMatchsAfterDate(DateTime.Now);



            return View(vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Votre page de description d’application.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Votre page de contact.";

            return View();
        }
    }
}
