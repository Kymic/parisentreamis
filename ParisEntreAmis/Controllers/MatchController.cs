﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ParisEntreAmis.Controllers
{
    public class MatchController : BaseController
    {


        private MatchService matchService = new MatchService();


        //
        // GET: /Match/


        [Authorize(Roles=Constants.ROLE_ADMIN)]
        public ActionResult Edit(int id)
        {

            Match match = matchService.GetById(id);

            MatchEditModel em = matchService.InitMatchEditModel(match);

            return View(em);
        }
        
        [Authorize(Roles = Constants.ROLE_ADMIN)]
        [HttpPost]
        public ActionResult Edit(MatchEditModel em, string scorersJson)
        {
            var scorers = new JavaScriptSerializer().Deserialize<IEnumerable<MatchGoal>>(scorersJson);


            //On recupere le match de la BDD pour le modifier
            Match match = matchService.GetById(em.MatchID);


            match.ScoreAwayTeam = em.ScoreAwayTeam;
            match.ScoreHomeTeam = em.ScoreHomeTeam;


            matchService.replaceScorers(match, scorers.ToList());
            matchService.saveChanges(match);

            ///
            /// TODO: CALL AJAX POUR METTRE UN CHARGEMENT EN PLACE
            /// POURRA PRENDRE BCP DE TEMPS
            /// CALCUL DES SCORES
            ///
            matchService.ComputeBetsForMatch(match);

            em = matchService.InitMatchEditModel(match);
            
            return View(em);
        }

        public ActionResult NextMatchs()
        {


            List<Match> matchs = matchService.GetMatchsAfterDate(new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day),100);

            return View(matchs);

        }



    }
}
