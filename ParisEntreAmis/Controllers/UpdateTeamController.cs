﻿using ParisEntreAmis.Models;
using ParisEntreAmis.Models.ViewModel;
using ParisEntreAmis.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParisEntreAmis.Controllers
{
    public class UpdateTeamController : BaseController
    {

        private UpdateTeamService updateTeamService = new UpdateTeamService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UpdateAllTeams(string country)
        {
            UpdateTeamsViewModel vm = new UpdateTeamsViewModel();

            List<Team> teams=new List<Team>();

             
            if(country=="FR")
            {
                teams = updateTeamService.UpdateTeamsForCountryAndCategory("FR", "Ligue 1", "http://www.sports.fr/football/ligue-1/clubs.html");
            }
            else if (country == "DE")
            {
                teams = updateTeamService.UpdateTeamsForCountryAndCategory("DE", "Bundesligua", "http://www.sports.fr/football/allemagne/clubs.html");
            }
            else if (country == "EN")
            {
                teams= updateTeamService.UpdateTeamsForCountryAndCategory("EN", "", "http://www.sports.fr/football/angleterre/clubs.html");
            }
            else if (country == "ES")
            {
                teams = updateTeamService.UpdateTeamsForCountryAndCategory("ES", "", "http://www.sports.fr/football/espagne/clubs.html");
            }
            else if (country == "IT")
            {
                teams = updateTeamService.UpdateTeamsForCountryAndCategory("IT", "", "http://www.sports.fr/football/italie/clubs.html");
            }



            vm.NbTeamsUpdated = teams.Count;

            vm.logs = updateTeamService.GetFullLogs();

            return View(vm);
        }

    }
}
