﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ParisEntreAmis.Models;
using ParisEntreAmis.Service;


namespace ParisEntreAmis.Controllers
{
    [Authorize]
    public class BetController : BaseController
    {
        private UserService userService = new UserService();

        private BetCircleService circleService = new BetCircleService();
        private BetService betService = new BetService();

        public BetController()
        {
            ViewBag.SaveSucess = false;

        }


        // public ActionResult Fix(int id)
        // {

        //    User user = userService.GetCurrentUser();

        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
            
        //    Bet bet = betService.GetForBetting(id, user);

        //    if (bet == null)
        //    {
        //        return HttpNotFound();
        //    }
            
        //    bet.DeadLine =  new DateTime (DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 9, 30, 0);
        //    betService.SaveChanges(bet);
        //    return View();

        //}

        public ActionResult Play(int id)
        {

            User user = userService.GetCurrentUser();

            if (user == null)
            {
                return HttpNotFound();
            }
            
            Bet bet = betService.GetForBetting(id, user);

            if (bet == null)
            {
                return HttpNotFound();
            }


            
            BetPlayedEditModel vm = betService.InitBetPlayedEditModel(bet, user);

            return View(vm);

        }

        [HttpPost]
        public ActionResult Play(BetPlayedEditModel BetPlayedEditModel)
        {
            User user = userService.GetCurrentUser();

            if (user == null)
            {
                return HttpNotFound();
            }

            //Get the bet to check the deadline
            Bet bet = betService.GetForBetting(BetPlayedEditModel.BetID, user);

            if (!betService.CheckIfUserCanPlay(bet, user))
            {
                return HttpNotFound();
            }

            betService.SaveBetPlayed(BetPlayedEditModel, user , bet);

            BetPlayedEditModel vm = betService.InitBetPlayedEditModel(bet, user);



            ViewBag.SaveSucess = true;

            return View(vm);
        }

        

    }
}
