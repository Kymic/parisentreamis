﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ParisEntreAmis.Controllers
{
    public class BaseController : Controller
    {

        private Stopwatch sw = null;

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            const string culture = "fr-FR";
            CultureInfo ci = CultureInfo.GetCultureInfo(culture);

            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            sw = new Stopwatch();
        }


        // Called before the action is executed
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            base.OnActionExecuting(ctx);
            // Start timing

            if (sw != null)
            {
                sw.Start();
            }
        }

        // Called after the action is executed
        protected override void OnActionExecuted(ActionExecutedContext ctx)
        {
            base.OnActionExecuted(ctx);
            // Stop timing

            if (sw != null)
            {
                sw.Stop();

                ViewData["_elapsedTime"] = sw.ElapsedMilliseconds;
            }
        }
     

    }
}
