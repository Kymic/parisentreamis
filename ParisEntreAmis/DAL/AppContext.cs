﻿using ParisEntreAmis.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.DAL
{    
    public class AppContext : DbContext
    {
        public AppContext()
            : base("ParisEntreAmis")
        {
           //Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppContext, Configuration>());
           // Database.SetInitializer<AppContext>(new AppContextInitializer());
            this.Configuration.LazyLoadingEnabled = true;

        }
        
        public DbSet<User> Users { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }


        public DbSet<Bet> Bets { get; set; }

        public DbSet<BetPlayed> BetPlayeds { get; set; }

        public DbSet<Match> Matchs { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Player> Players { get; set; }
        
        public DbSet<BetCircle> BetCircles { get; set; }

        public DbSet<MatchGoal> MatchGoals { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        public DbSet<Invitation> Invitations { get; set; }

        public DbSet<Parameter> Parameters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();



             //----------------------------------------------------------------
            //Creating a Association (Intermediate Table) which
            //will hold M2M relations from User to Circle.
            //----------------------------------------------------------------
            modelBuilder.Entity<User>()
                .HasMany(c => c.BetCircles)
                .WithMany(s => s.Users)
                .Map (mc =>
                    {
                       mc.ToTable("UsersInCircle");
                       mc.MapLeftKey("BetCircleID");
                       mc.MapRightKey("UserID");
                   });


            //On désactive l'auto-increment sur l'ID des joueurs et des Teams
            //On garde ceux du site http://en.soccerwiki.org/downloads.php?action=country&countryId=FRA
            //pour pouvoir les associer aux différentes équipes
            modelBuilder.Entity<Player>().Property(m => m.PlayerID)
                            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<Team>().Property(m => m.TeamID)
                            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);




        }
    }


    internal static class ContextPerRequest
    {
        internal static AppContext Current
        {
            get
            {
                if (!HttpContext.Current.Items.Contains("myContext"))
                {
                    HttpContext.Current.Items.Add("myContext", new AppContext());
                }
                return HttpContext.Current.Items["myContext"] as AppContext;
            }
        }
    }

    public class WebContext : IDBContext
    {
        public AppContext GetContext()
        {
            return ContextPerRequest.Current;
        }
    }
}