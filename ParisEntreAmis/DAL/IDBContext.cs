﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParisEntreAmis.DAL
{
    public interface IDBContext
    {

        AppContext GetContext();
    }
}