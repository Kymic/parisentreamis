﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ParisEntreAmis.Extensions
{
    public static class HtmlExtensions
    {
        /// <summary>
        /// Display url with class = active regarding the current controler and action name
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        public static MvcHtmlString MenuItem(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName)
        {
            string currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            string currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
            string resultFormat = "<li class=\"{0}\">{1}</li>";
            string cssClass = string.Empty;
            if ((actionName == currentAction || String.IsNullOrEmpty(actionName)) && controllerName == currentController)
            {
                cssClass = "active";
                //return htmlHelper.ActionLink(
                //    linkText,
                //    actionName,
                //    controllerName,
                //    null,
                //    new
                //    {
                //        @class = "active"
                //    });
            }
            string result = string.Format(resultFormat
                , cssClass
                , htmlHelper.ActionLink(linkText, actionName, controllerName).ToHtmlString()
                );
            return new MvcHtmlString(result);
        }
    }
}